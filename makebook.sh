#!/bin/bash

rm *.log
rm *.ilg
rm *.ind
rm *.idx
rm *.dat
rm *.aux
rm *.logo
rm ./Lieder/*.aux

java -jar ./Lieder/ChangeAkk.jar
pdflatex fullbook.tex
java -jar russisch_transliteral.jar
pdflatex fullbook.tex

rm -R *.log
rm -R *.ilg
rm -R *.ind
rm -R *.idx
rm -R *.dat
rm -R *.aux
rm *.logo
rm ./Lieder/*.aux
