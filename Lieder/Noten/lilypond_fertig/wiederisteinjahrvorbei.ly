\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m g d d e:m d e:m e:m g d b:m e:m g d e2:m d e1:m
}

mel = \relative c' {
  \global
  e4 b c d |
  e2. c4 |
  b2. r4 |
  r2. d4 |
  e e8 e b'4 b | 
  a a fis d |
  e2 r2 
  r2. e4 |
  b'2 a4 g |
  a2 g4( fis) |
  a2 g4( fis) |
  e2 e4( fis) |
  g2 b |
  a2 d,4 fis |
  e4. g8 fis4 d |
  e2 r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Wie -- der ist ein Jahr vor -- bei;
  Wir tref -- fen uns wie -- der am ver -- trau -- ten Ort.
  Der Wald läd uns ein, es rauscht der Bach, die Bäu -- me 
  knarrn und das Feu -- er lockt her -- bei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






