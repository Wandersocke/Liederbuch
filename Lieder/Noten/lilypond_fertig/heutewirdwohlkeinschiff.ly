\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 6/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c2 c4 a:m a2:m d:m d4:m g g2 c c4 a:m a2:m d:m 
  d4:m g g2 e:m e4:m a:m a2:m f1 f2 c c4 g g2 
  c4. a8:m d:m g
}

mel = \relative c' {
  \global
  e4 e8 e e e |
  e d c( c4) c8 |
  d4 d8 d d c |
  d4.( d4.) |
  e8 e e e e e |
  d c4( c) c8 |
  d d d d4 c8 |
  b b c d4 d8 |
  e4 e8 e e e |
  e d c( c) c c |
  f4 f8 f g f |
  c4.( c4.) |
  e4. c8 c c |
  d4 c8 b c d |
  c4.( c4.) \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Heu -- te wird wohl kein Schiff mehr gehn und
  kei -- ner geht vor die Tür. Al -- le sind heu -- te ver -- schüch -- tert,
  nur ich bin es nicht und das liegt an dir. Am Fen -- ster fliegt 
  ei -- ne Kuh vor -- bei, da kommt je -- de Hil -- fe zu spät.
  Ein Glas auf die Kuh und eins auf die See.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






