\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 6/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d2 d4 a:7 a2:7 b:m b4:m a:7 a2:7
  d d4 b:m b2:m g g4 a:7
}

melody = \relative c' {
  \global
	 fis4. ^\markup {\upright  "Kehrvers"} a | % 2
	 e  a | % 3
	r8   d,  d  d  e  fis | % 4
	 e2. | % 5
	 fis4.  a | % 6
	 b8  fis4 ~   fis4. | % 7
	r8   g  g  g  a  b | % 8
	 a2. 
	\bar "|."
	
}

verse = \lyricmode{
	 Sei   nicht   trau -- rig,   wenn   der   Au -- gen -- blick  
	 flieht,   der   ist   glück -- lich,   der   den   Son -- nen  
	 -- schein   sieht.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}