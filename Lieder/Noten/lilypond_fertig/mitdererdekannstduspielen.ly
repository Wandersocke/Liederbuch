\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  e1:m c1 d1 g2 b2:7 a1:m 
  e1:m b1:7 e1:m a1:m e1:m a2:m c2 f2 f2 b1:7 d1:7
  g1 d1 e1:m b1:m c1 g1 a1:7 d1 b1:7 e1:m
}

melody = \relative c' {
  \global
\repeat volta 2 {
	 b4  e  fis  g | % 2
	 c,  e  fis  g | % 3
	 d  fis  g  a | % 4
	 b  a  b2 
	}
	| % 5
	 c4  c  b  a | % 6
	 b  b  a  g | % 7
	 a  a  g  fis | % 8
	 g  a  b2 | % 9
	 c4  c  b  a | % 10
	 b  b  a  g | % 11
	 \numericTimeSignature
	 a  a  g  g  \time 2/2  s4 fis  fis s4
	 \time 4/4  b1 | % 14
	r1  \bar "||" \break
	\repeat volta 2 {
	 b4 ^\markup {\upright  "Kehrvers"} b  c  d | % 16
	 d  a2 r4 |  g2  a4  b | % 18
	 fis2 r2  
	|
	 e4  e  fis  g | % 20
	 g  d2 r4  
	}
	\alternative {
	{
	 a'2  a4  b | % 22
	 a2. r4  
	}
	
	{
	| % 23
	 a2  g4  fis | % 24
	 e2. r4  
	}}
	
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Mit   der   Er -- de   kannst   du   spie -- len,   spie  
	 -- len   wie   der   Wind   im   Sand,   Mit   der   Er --   
	 de   kannst   du   bau -- en,   bau -- en   dir   ein  
	 schö -- nes   Haus,   doch   du   soll -- test   nie   ver  
	 -- ges -- sen:   Ein -- mal   ziehst   du   wie -- der   aus.  
	 Ei -- ne   Hand -- voll   Er -- de,   schau   sie   dir  
	 an.   Gott   sprach   einst:   Es   wer -- de!   Den -- ke  
	 da -- ran.   Den -- ke   da -- ran.  
}
verse = \lyricmode{
	 und   du   baust   aus   dei -- nen   Träu -- men   dir  
	 ein   bun -- tes   Träu -- me -- land. 
      }

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
