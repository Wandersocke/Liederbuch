\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  g2 c d g g c d g g e:m d e:m d c g d g4
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 d8  d4  g8 | % 2
	 e4  e8  g | % 3
	 fis4  g8  a | % 4
	 g4  d | % 5
	 b'8  b4  g8 | % 6
	 c4  c8  b | % 7
	 a4  a8  b | % 8
	 g2 
	}
	
	\repeat volta 2 {
	s4 ^\markup {\upright  "Kehrvers"} s8  d | % 10
	 e4  e8  b' | % 11
	 a4.  d,8 | % 12
	 e  e4  b'8 | % 13
	 a4  g8  fis | % 14
	 g4  fis8  e | % 15
	 d4  g8  b | % 16
	 a4  b8  a | % 17
	 g4.  
	}
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Was   uns   lässt   fah -- ren   kann   nicht   je -- der  
	 füh -- len,   kann   nur   ver -- steh'n,   wer   mit   uns  
	 ein -- mal   zog.   Es   liegt   uns   im   Blut   und  
	 treibt   uns   da -- von,   im -- mer   wie -- der   da  
	 -- von,   im -- mer   wie -- der   da -- von.  
}
verse = \lyricmode{
	 Wer   sei -- ne   Stirn   mag   an   Quell -- was -- ser  
	 küh -- len,   kön -- nte   ge -- ra -- ten   in   un  
	 -- ser -- en   Sog.                    
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
