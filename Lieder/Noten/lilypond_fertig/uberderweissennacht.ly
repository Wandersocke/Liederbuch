\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  e1:m a2:m e:m b1 e:m b e:m a:m b
}

chordNamesz = \chordmode {
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  e:m a2:m e:m b1 e:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    e2 b4 b |
    c4. d8 e4 g |
    fis4. g8 a( g) fis4 |
    e1 |
  }
  dis4. e8 fis4 fis |
  e4. fis8 g4 g |
  a4. b8 c4 c |
  b4. c8 b2 | \bar "||" \break
}

melz = \relative c' {
  \key e \major
  \bar "|:"
  \repeat volta 2 {
    e2 ^\markup {\upright  "Kehrvers"} b4 b |
    cis4. dis8 e4 gis |
    fis4. gis8 a( gis) fis4 |
    e2. r4 |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Ü -- ber der wei -- ßen Nacht steht
  hell der Gro -- ße Bär.
  Schnei -- dend klingt der Mö -- we Schrei, Be -- glei 
  -- te -- rin zur Wal -- fisch -- jagt.
}

verse = \lyricmode {
  Schau -- kelt die Dü -- nung
  sacht das Schiff auf wei -- tem Meer.
}

versz = \lyricmode {
  Bald fährt das Schiff nach Süd im we -- hen -- den
  Pas -- sat.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff
      \new Voice <<
        \mel
      >>
   
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
      
    >>
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
  %2. System wegen Tonartänderung nach Zeilenumbruch
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNamesz}
      \new Staff
      \new Voice <<
        \melz
      >>
   
      \addlyrics {\set fontSize = #+2 \versz}
      
    >>
   \layout {
     ragged-right = ##f %Zeile auf ganze Seitenbreite strecken
     \override Score.TimeSignature #'stencil = ##f %Taktart ausblenden
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






