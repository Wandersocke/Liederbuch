\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c1 d2:m g:7 c f g c a:m f c g g a:m a:m d:m g1 a:m
}

mel = \relative c' {
  \global
  e4 e g g |
  f8( g) f e d2 |
  e4 g c c |
  b8 c b a g4 g8( f) |
  e4 e a a |
  g8( f) e4 d2( |
  d) c4 c |
  e e f f8( e) |
  d2. c8 b |
  c2. r4 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Gel -- be Blät -- ter fal -- len im Wind,
  wir -- beln auf und flie -- hen dann ge -- schwind 
  auf grau -- er Stra -- ße im -- mer -- zu,
  fin -- den erst im Win -- ter -- schnee ih -- re Ruh.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






