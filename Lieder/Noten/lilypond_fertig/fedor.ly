\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key f \major
  \time 4/4 
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d2:m d2:m d2:m d2:m a2 a2 d2:m d2:m
  f f g:m g:m a a d:m d:m f2 c2
  d2:m a2 d2:m c2 f f d2:m a d
}

melody = \relative c' {
  \global
	 d4.  e8  f4  d | % 2
	 a'  f  e  d | % 3
	 c4.  e8  a4  c, | % 4
	 d2. r4  | % 5
	 f4.  g8  a4  f | % 6
	 bes  a  g  f | % 7
	 e4.  e8  a4.  a8 | % 8
	 d,2. r4  | % 9
	 a'  a  g2 | % 10
	 f4  f  e2 | % 11
	 d4  d  c  c' | % 12
	 a  a8  a  a  a  a4 | % 13
	 f  f  e  e | % 14
	 d1 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Fe -- dor   rei -- tet   schwar -- zen   Rap -- pen,   und  
	 die   Nacht   ist   lind.   Durch   die   Wäl -- der   ü  
	 -ber   Fel -- der   singt   so   sehr   der   Wind.   Rei  
	 -te   zu,   rei -- te   zu,   fin -- dest   kei -- ne  
	 Ruh,   fin -- dest   kei -- ne   Ruh,   fin -- dest   kei  
	 -ne   Ruh.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
