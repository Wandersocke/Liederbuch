\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key c \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  c2 a4:m e:m a4.:m f4. f2. g2 e1:m
  c2 a4:m e:m a4.:m f4. f4 f2 g2 c1
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  g'4 g a b |
  c b8 a4. r4 |
  a4 a g d |
  e2. r4 |
  g g a b |
  c b8 a4. r4 |
  a4 a g8 a4. |
  g2. r4
  \bar "||"
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"3. " Tau -- sen -- de zer -- stampft der Krieg, -- ei -- ne Welt ver -- geht.
  Doch des Le -- bens Blü -- ten -- sieg leicht im Win -- de weht.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






