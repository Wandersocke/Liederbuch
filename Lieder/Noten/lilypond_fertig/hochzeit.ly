\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key f \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s8 d1:m c2 f g4:m c f c d2:m a4 d:m
  d1:m d:m g2:m a d:m g4:m a d2:m c
  f c f a a c f c f c f a
  g:m d4:m a d2:m d1:m
  c2 f g:m d:m a d:m a d:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \partial 8
  a'8^\markup {\upright  "Intro"} |
  aes a r a aes a r a |
  g a c bes a4 r |
  d c8. g16 bes8 a g e |
  d des16 d f8 e e4 d | \bar "||" \break
  \repeat volta 2 {
    d8 a' aes a bes a f d |
    d a' aes a bes a f d |
    g16 g8 g8. g8 f f e16 e8. |
  }
  \alternative {
    {
      d4 r bes' a |
    }
    {
      d,4 r r2 |
    }
  }
  r8 c' c c bes16 a8. g8 e |
  a4 r a r |
  r8 a a16 a8. g8 f e d |
  f4 r g r |
  r8 c c c b a g16 e8. |
  a4 r a r |
  g16 g8 g8. g8 f f e e |
  \time 2/4
  d8 \bar "||" a'^\markup {\upright  "Kehrvers"} aes a |
  \repeat volta 2 {
    \time 4/4
    aes4 a aes a8. a16 |
    g8 g c bes a4 r |
    g8 a c bes a16( f8.) d8 a' |
  }
  \alternative {
    {
      a g f e d a' aes a |
    }
    {
      a g f e d4 r |
    }
  } \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  " "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "
  \set stanza = #"1. " Die -- ses klei -- ne Winz -- or -- ches -- ter gönnt euch Ru -- he
  et -- was spä --ter al -- so, Gei -- ger, fröh -- lich soll es sein! Hej, hej! " "
  Drau -- ßen, da dun -- kelt's lan -- ge schon, oh,
  nach Hau -- se will noch kei -- ner geh'n, ach!
  Ein lee -- res Wein -- fass ist der Lohn, oh,
  ir -- gend -- ei -- ner wird nach Neu -- em seh'n.
  Und jetzt kommt's:
  Hoch -- zeit, Hoch -- zeit, im Le -- ben nur ein -- mal!
  Man -- che ma -- chen's öf -- ter, doch ist das nicht nor -- mal. 
  Und noch -- mal:
  ist das nicht nor -- mal.
}

verse = \lyricmode {
  " "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "
  Leu -- te, macht nun Platz zum Tan -- zen, Mu -- si -- kan -- ten, quält die Tas -- ten
  singt für eu -- re Gäs -- te, haut mal " "" "" " rein!
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
          \addlyrics {\set fontSize = #+2 \verse}
        >>
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






