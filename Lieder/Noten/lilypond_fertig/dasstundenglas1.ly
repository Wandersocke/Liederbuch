\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 6/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d2 d4 b:m b2:m a:7 a4:7 d1 d2 b:m b4:m g g2 a:7 a4:7 d d8 g g4 a:7 a8:7 d2 d4 g g8 a a4 d1 d8 b2:m b4:m a:7 a2:7 d
}

melody = \relative c' {
  \global
	 a'4  a8  a4  g8 | % 2
	 fis4  d8  d4  d8 | % 3
	 e4  e8  e  fis  g | % 4
	 fis2. | % 5
	 a4  a8  a4  a8 | % 6
	 b4  fis8  fis4. | % 7
	r8   g  g  g  a  b | % 8
	 a4. ~   a8  a  g | % 9
	 fis4  fis8  g4  g8 | % 10
	 a  a  g  fis4. | % 11
	 fis4  fis8  g4  g8 | % 12
	 a  a  g  fis4. | % 13
	 a4  a8  a4  g8 | % 14
	 fis4  d8  d4  d8 | % 15
	 e4  e8  e  d  cis | % 16
	 d2. 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Wei -- ßer   Sand   um -- hüllt   von   Glas,   von   Stand  
	 -- ge -- häu -- se   und   Zinn.   Rie -- selt   oh -- ne  
	 un -- ter -- lass   Traum   und   Stun -- den   da -- hin,  
	 und   so   lau -- los   wie   die   Sand -- säu -- le  
	 fällt,   zählt   es   die   Se -- kun -- den   der   Welt,  
	 ist   dann   sei -- ne   Zeit   her -- um,   so   dreht  
	 man   es   wie -- der   um.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
