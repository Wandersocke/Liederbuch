\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  b2:7 e:m b:7 e:m b:7 e:m b:7 e:m
  a:m e:m b:7 e:m a:m e:m b:7 e:m
  d g d g d g d g c g d g c g d g
  g c1 g c g a:m e:m b:7 e2:m
}

mel = \relative c' {
  \global
  b4 b e4. r8 |
  fis4 fis g4. r8 |
  b,4 b e e |
  fis fis g g | \break
  a a g g |
  fis fis e e |
  r1 |
  r | \break
  d4 d g4. r8 |
  a4 a b4. r8 |
  d,4 d g g |
  a a b b | \break
  c c b b |
  a a g g |
  r1 |
  r | \break
  \repeat volta 2 {
    b4 b c4. r8 |
    c4 c b4. r8 |
    b4 b c c |
    c c b b | \break
    b b a a |
    a a g g |
    g g fis fis |
    dis dis e e |
  }
}

vers = \lyricmode {
  \set stanza = #"1. + 3. " Stin a -- pa-, stin a -- pa-, 
  Stin a -- pa -- no yi -- to -- nit -- sa
  Stin a -- pa -- no yi -- to -- nit -- sa.
  M'a -- ga pa-, m'a -- ga pa-,
  M'a -- ga pa -- ne dio ko -- rit -- sa,
  Stin a -- pa -- no yi -- to -- nit -- sa.
  Ma e -- go, ma e -- go, ma e -- go po -- na -- o yia -- li,
  Ma e -- go, po -- na -- o yia -- li mya gor -- go -- na
  sta -- kro yia -- li.
}

verse = \lyricmode {
  \set stanza = #"2. " Fi -- lak -- to, fi -- lak -- to, fi -- lak -- to
  me ta -- yio ksi -- lo Fi -- lak -- to, me ta -- yio ksi -- lo.
  Pou na vro, pou na vro, pou na vro yia na tis stil -- lo,
  Fi -- lak -- to, me ta -- yio ksi -- lo.
  Fi -- lak -- to, fi -- lak -- to, fi -- lak -- to na ti fi -- la -- i
  Fi -- lak -- to na ti fi -- la -- i kye ass mi me a -- ga -- pa -- i.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






