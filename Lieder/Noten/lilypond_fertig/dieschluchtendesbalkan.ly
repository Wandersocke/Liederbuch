\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  s4 e2:m g d e:m g d b1:7 e2:m g d e:m g d b:7 s
  g1 d e:m b:7 e2:m b:7 e:m a:m e:m b:7 e:m
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  e8 e4 e8 g g4 g8 |
  fis2 e |
  g8 g4 g8 a d4 c8 |
  b2. r4 |
  e,8 e4 e8 g g4 g8 |
  fis2 e |
  g8 g4 g8 a d4 c8 |
  b2. \bar "||" \break
  \repeat volta 2 {
    r4 ^\markup {\upright  "Kehrvers"} |
    g8 g g b d b4 g8 |
    a1 | \break
    e8 e e g b g4 e8 |
    fis2. b4 |
    e,8 e4 e8 fis fis4 fis8 |
    g g4 g8 a a4 a8 |
    b g4 e8 fis g4 fis8 |
    e2. s4 |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Die Schluch -- ten des Bal -- kans zu zwin -- gen,
  die die Le -- gen -- de er -- wähnt, mit den Dä -- mo -- nen zu rin -- gen,
  die die -- se Fel -- sen ge -- zähmt:
  Rie -- fen wir die heim -- li -- che Schar, die von Göt -- tern aus -- er -- wählt war
  zu su -- chen, zu wä -- gen, zu zwei -- feln, zu hof -- fen, 
  zu wis -- sen, was wirk -- lich, was wahr.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






