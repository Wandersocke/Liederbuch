\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key c \major
}

mel = \relative c {
  \global
  e16 fis g a b g fis g fis d e \bar "|"
}

\book {
  \score {
    <<
      
    \new TabStaff
    { \mel }
   >>
   \midi{ } 
   \layout {
    \context {
      \Score

    }
    }
  }
}






