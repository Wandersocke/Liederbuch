\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 d1:m d2:m a d:m d1:m a
  f f2 c d:m c a a:7 f f1 c2
  d:m a d:m
}

mel = \relative c' {
  \global
  \partial 4
    d4 |
    d a' |
    a a |
    a g8( f) | 
    e4. e8 |
    f4 f |
    a a |
    a g8( f) |
    f2 |
    e4. f8 |
    f4 c' |
    c c |
    c bes8( a) |
    g4. g8 |
    a4 a |
    g8 g f g |
    a2 |
    a4. f8 |
    f4 c' | 
    c c |
    c bes8( a) |
    g4. g8 |
    a4 a |
    g8 g f e |
    d2 |
    d4 \bar "|."
}

vers = \lyricmode{
 \set stanza = #"1. " Der Tag ent -- glei -- tet schat -- ten -- sacht,
 die letz -- te Hel -- le geht zur Nei -- ge.
 Auf Eu -- len -- schwin -- gen kommt die Nacht
 und nis --  tet drau -- ßen im Ge -- zwei -- ge.
 Auf Eu -- len -- schwin -- gen kommt die Nacht
 und nis -- tet drau -- ßen im Ge -- zwei -- ge.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
