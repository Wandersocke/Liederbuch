\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a2:m f f8 g g4 a1:m f2 f8 g 
  g4 g2 a:m f f8 g g4 d1:m a2:m 
  a8:m e:7 e4:7 a:m
}

mel = \relative c' {
  \global
  <a a' c>4. r8 |
  <a' c>4. r8 |
  r8 <g b> <g b>( <d g>) |
  <e a>4. r8 |
  r8 <a, a' c> <a a' c> <a a' c> |
  <a' c>4 <a c> |
  r8 <g b> <g b> <d g> |
  <e a>4. r8 |
  r8 <a, a' c> <a a' c> <a a' c> |
  <a' c>4( <a c>) |
  r4 <g  b>8 <a c> |
  <e a>4. r8 |
  r8 <d a> <e b> <f c> |
  <e a>4 <c e> |
  r8 <b e> <c c'> <b b'> |
  <a a'>4. r8 \bar "|." |
}

vers = \lyricmode {
  \set stanza = #"1. " 
  Степь, степь, степь кру -- гом,
  два ста -- рых му -- ла ве -- зут фур -- гои.
  Из го -- ро -- дов, от су -- еты,
  на Даль -- ний За -- иад у -- хо -- дим мы.
}

verse = \lyricmode {
  \set stanza = #"1. " 
  Step, step, step kru -- gom, 
  dwa sta -- rych mu -- la we -- sut fur -- gon.
  Is go -- ro -- dow, ot su -- jety, 
  na Dal -- ni Sa -- pad u -- cho -- dim my. 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






