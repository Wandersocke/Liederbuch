\version "2.14.2"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 s4 e2:m e4:m b:7 e2:m e4:m d:7 g2 d g g4 
 e:7 a1:m d:7 g2: g4 b:7 e2:m
}

melo = \relative c' {
 s1 s s s s4 | 
      c'4. b8 a4 e |
    a a r g8 a |
    b4. a8 g4 fis |
    e2.
}

mel = \relative c' {
  \global
  \partial 4
  e8 fis |
  g4 e g b8 (a) |
  g fis e2 d8 d |
  g4 g a d8 (c) |
  b2 r4 
  \break
  \repeat volta 2 {
    e,8 e |
    e'4. d8 c4 b |
    c c r b8 c |
    d4. c8 b4 a |
    e'2.
  }
}

vers = \lyricmode{
 \set stanza = #"1. " Durch die Step -- pe, durchs Ge -- bir -- ge zog
 uns -- re küh -- ne Di -- vi -- sion,
 hin zur Küs -- te die -- ser wei -- ßen,
 heiß um -- strit -- te -- nen Bas -- tion.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \melo \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
