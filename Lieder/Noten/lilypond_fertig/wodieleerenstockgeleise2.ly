\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key d \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  b1:m a fis:m a2 g1 a g fis2:m b:m g4 a d1
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  b'4^\markup {\upright  "3. + 5. Strophe"} cis d d |
  cis b a fis |
  a b cis a |
  \time 2/4
  e fis |
  \time 4/4
  d2 r | \break
  \repeat volta 2 {
    e4 fis g a |
    g fis e d |
  } 
  e fis d a |
  \time 2/4
  b cis |
  \time 4/4
  d2 r \bar "|."
  %e8 g bis e bis g 
  %d( e) g e' b g 
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"3. " Willst du mich am A -- bend se -- hen, war -- te nicht im Stie -- gen -- haus;
  komm im ers -- ten leich -- ten Schat -- ten, schwach noch se -- hen lässt hi -- naus.
}

verse = \lyricmode {
 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ der die Hol -- pern längst der Lat -- ten,
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
    \new Staff
    \new Voice <<
      \mel
   >>
   
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
      
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}





