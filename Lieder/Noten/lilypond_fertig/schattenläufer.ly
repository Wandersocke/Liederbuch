\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 a1:m e d:m a:m d:m a:m e e2 e4 
  \set chordChanges = ##f
  e4 
  \set chordChanges = ##t
  f e2 e4
  a2:m a4:m c2 c4 f2 f4 e2 e4 a2:m a4:m c2 c4 f2 f4
  e2 e4 d2:m d4:m a2:m a4:m g2 g4 c2 c4 d2:m d4:m 
  a2:m a4:m e2 e4 a2:m a4:m
  
}

melody = \relative c' {
  \global
	 a8  b | % 2
	 c  b  a  b  c  b  a  c | % 3
	 b  c  b  a  b r8   b  c | % 4
	 d  c  b  c  d  c  b  d | % 5
	 c  b  a  b  c r8   a  a | % 6
	 f'  e  d  e  f  e  d  f | % 7
	 e  f  e  d  e r8   c  c | % 8
	 b  b  b  b  b  a  gis  a | % 9
	 b2. r4 \bar "||" | % 10
	 r4 ^\markup {\upright  "Kehrvers"} r4 r4 c8 b 
	\time 6/8  a4  a  c8  d | % 12
	 e4  e  d8  c | % 13
	 f4  c  d | % 14
	 e2  c8  b | % 15
	 a4  a  c8  d | % 16
	 e2  d8  c | % 17
	 f4  c  d | % 18
	 e2  e8  e | % 19
	 a4  a  a8  g | % 20
	 e4  a,  e'8  e | % 21
	 g4  d  g | % 22
	 e2  e8  e | % 23
	 f4  f  e8  d | % 24
	 e(  c)  a4  a8  a | % 25
	 b4  c  b | % 26
	 a2 r4  
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Durch   die   Stras -- sen,   durch   die   Gas -- sen   geht  
	 ein   Weg   fort   durch   die   Stadt.   Gno -- me,   Zwer -- ge, 
	    Elf -- en,   Men -- schen   trifft   sich   dort   was  
	 Na -- men   hat.   Le -- ben,   han -- deln,   Fes -- te  
	 fei -- ern,   Feu -- er   bren -- nen   Tag   und   Nacht,  
	 bun -- tes   Trei -- ben   auf   den   Plät -- zen   hält  
	 hier   Wacht.   Je -- doch   ei -- nes   bleibt   ver -- wo -- ben, 
	    tief   in   Schat -- ten   ge -- hüllt.   Und   du  
	 hast   wohl   kaum   den   Blick   aus   dem   Dun -- kel  
	 ge -- fühlt,   schlei -- chend,   wo -- gen   und   nicht   greif -- bar  
	    ist   es   stets   nah   bei   dir,   dei -- nen  
	 Bli -- cken   so   ver -- bor --  -- gen,   Schat -- ten -- läu -- fer  
	    sind   wir.  
}

verse = \lyricmode {
  In den Gas -- sen lau -- ern Die -- be, Lü -- gen gehn von Mund zu Mund,
  in Ta -- ver -- nen -- eck -- en flüs -- tern still Ver -- rät -- ter dunk -- le
  Kund. Dort die Frau gibt sich für Sil -- ber ih -- re Kin -- der
  steh -- len Brot, zwi -- schen Häu -- sern zieh -en Bett -- ler 
  vol -- ler Not.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \set chordNameLowercaseMinor = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+1 \vers}
      \addlyrics {\set fontSize = #+1 \verse}
    >>
    \layout { }
  }
}