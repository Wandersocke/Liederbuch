\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  a1:m d1:m a1:m e1
  a1:m d1:m a2:m e2 a1:m
  a1:m a1:m c1 c1 c2. g4 g1 a1:m
}

melody = \relative c' {
   \global
	 a'4  a  a8  g  f  e | % 2
	 d4.  e8  f4  g | % 3
	 e2 ~   e8  d  c  d | % 4
	 e1 
	\repeat volta 2 {
	| % 5
	 a4  a  a8  g  f  e | % 6
	 d4.  e8  f4  g | % 7
	 e8  c4  c8  c4  b | % 8
	 a1 
	}
	
	\repeat volta 2 {
	| % 9
	 e'2 ~   e8  d  c  d | % 10
	 << %2te Stimme I
	 { e1 | e2 ~}
	 \new Voice = mel {
	  \voiceTwo
	  e4 d4 c4 d4 | e2 ~
	 }
         >>
          e8  d  c  d | % 12
	 << %2te Stimme II
	   { e1 | g2. }
	   \new Voice = melo {
	    \voiceTwo
	    e4 d4 c4 d4 | g2.
	 }
	 >>  
	 a4 | % 14
	 b  g  c8  b4. | % 15
	 a1 
	}
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Je -- den   Mor -- gen   ruft   das   jun -- ge   Le  
	 -ben,   was   der   Tag   ihm   bringt,   bis   der   Zwei  
	 -fel   all   die   gu -- ten   Ta -- ten   schließ -- lich  
	 zu   Bo -- den   ringt.   Ge -- stern   ist   vor -- bei,
	  mor -- gen   ein -- er -- lei,   _ heute   noch,   da  
	 sind   wir   jung.   
}
\book {
  \score {
    << 
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \new Lyrics \lyricsto "mel" {
	    Ges -- tern ist vor -- bei,
	 }
	 
      \new Lyrics \lyricsto "melo" {
	    mor -- gen ein -- er -- lei,
	 }
	 
      %  \addlyrics { \verse}
    >>
    \layout { }
  }
}
