\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key c \major
  \time 6/8
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s8 a4.:m e4. a4.:m a4.:m a4.:m g4. c4. e4.
  d4.:m g4.:7 c4. a4.:m d4.:m e4. 
  a4.:m c4. d4.:m d4.:m e4.
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \partial 8
  a8 |
  a b c d c b |
  a4.( a4) a8 |
  a b c d c d |
  e e4 r e8 |
  d8. e16 f8 f e d |
  e( d) c a4 a8 |
  d8. e16 f8 f e d |
  a' e d c b a |
  b c b d c c |
  b b4 r \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Die Kra -- ni -- che flie -- gen im Keil,
  so trot -- zen sie bes -- ser den Win -- den, so tei -- len sie 
  bes -- ser die Kräf -- te, weil die Stär -- ker -- en flie -- gen
  im vor -- der -- en Teil und die Schwä -- che -- ren, die flie -- gen
  hin -- ten.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






