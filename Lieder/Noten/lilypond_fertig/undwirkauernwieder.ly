\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key g \major
  \time 4/4
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
 s4 e2:m b2 e4:m c4 e4:m g4
 a2:m d2:7 g2 b2:7 g2 d2 e2:m b2 
 e4:m a4:m e4:m b4:7 e4:m a4:m e4:m
}

melody = \relative c' {
  \global
  \repeat volta 2 {
	 b8  b | % 2
	 e4  e  e  fis | % 3
	 g8  fis  g  fis  e4  e8  d | % 4
	 c4  c8  c  d4  d | % 5
	 b2  b4   
	}
	
	\repeat volta 2 {
	 e8 ^\markup {\upright  "Kehrvers"} g | % 7
	 b4  g8  b  a4  fis8  a | % 8
	 g4  e8  g  fis2 | % 9
	 e4  e8.  e16  e8  e  dis  dis | % 10
	 e2  e4  
	}
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Und wir kau -- ern   wie -- der   um   die   hei -- ße
	Glut   und   er -- zäh -- len   vom   A -- ben -- teu -- er.   
	Dass   die   Nei -- der   ver -- dammt   und  
	 die   Spie -- ßer   ver -- flucht,   sie   uns   ge -- hemmt  
	 viel   tau -- send   Ma -- le.  
}
verse = \lyricmode{
	 Denn der   wil -- de   Bal -- kan   zün -- det   un -- ser   
	 Blut,   und   wir   sin -- gen   am   La -- ger -- feu -- er:
}

\book {
  \score {
    <<  
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
