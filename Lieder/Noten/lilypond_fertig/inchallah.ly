\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s2 a1:m g  g a:m a:m g g a2:m
}

mel = \relative c' {
  \global
  \partial 2
  s8 a a c |
  e2( e8) e f e |
  d2( d8) b b c |
  d4 b a g |
  a2 r8 a a c |
  e2( e8) e f e |
  d2( d8) b b c |
  d4 b a g |
  a2 r8 \bar ".|"
}

vers = \lyricmode {
  \set stanza = #"1. " Wie lan -- ge schon war es mein Traum, die Rei -- se in den O -- ri -- ent.
  Das Mor -- gen -- land nun sah ich kaum, das man von Mär -- chen -- bü -- chern kennt.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    }
  }
}
