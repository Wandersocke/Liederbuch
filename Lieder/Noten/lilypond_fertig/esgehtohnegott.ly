\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s1 d d e:m e:m a a d d g e2:m g d1 g d e:m a
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    r2 ^\markup {\upright  "Kehrvers"} fis4 e |
    d d8 d( d2) |
    r2 fis4 g |
    fis e8 e( e2) |
    r2 e4 d |
    cis cis cis2( |
    cis4) d e fis |
    d1
  }
  
  \break
  
  \repeat volta 2 {
    fis4 a g fis |
    e d b d |
    e d b d |
    fis e fis2 |
    g4 b a g |
    fis a g fis |
    e fis g fis |
    e1
  }
}

vers = \lyricmode {
  Es geht oh -- ne Gott in die Dun -- kel -- heit,
  a -- ber mit Ihm ge -- hen wir ins Licht.
  \set stanza = #"1. " Als die Welt noch jung war, 
  noch die kla -- ren Spu -- ren Got -- tes trug,
  woll -- ten Men -- schen schon so 
  klug und e -- wig sein wie Er.
}

verse = \lyricmode {
  Sind wir oh -- ne Gott, macht die Angst sich breit,
  a -- ber mit Ihm fürch -- ten wir uns nicht.
  Und be -- vor sie es ver -- such -- ten,
  fühl -- ten sie sich stark ge -- nug,
  doch wo -- hin es führ -- te, merk --ten 
  sie erst hin -- ter -- her.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






