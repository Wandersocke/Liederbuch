\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1 fis b2:m g a1 b4:m g a2 d a
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    a4 fis' g8 fis e d |
    fis4 r8 cis8 cis4 e | \break
  }
  \alternative {
    {
      d fis b b |
      a e r2 |
    }
    {
      d4 g e4. d8 |
      d4 d r2 |
      \bar "|."
    }
  }
  
}

vers = \lyricmode {
  \set stanza = #"1. " Ich will ge -- gen das Ge -- läut der Leu -- te
  mein Ge -- schwei -- ge stim -- men, mein Ge -- schwei -- ge stim -- men.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	%\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      %\remove "Volta_engraver"
    }
    }
  }
}






