\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 e2:m g d e:m e:m d e:m e:m e:m g a e:m e:m d d e:m e:m e:m d e:m d e:m d e:m
}

melody = \relative c' {
  \global
	 e2 | % 2
	 g4  g | % 3
	 fis2 | % 4
	 e | % 5
	 b' | % 6
	 a8  g  fis  g | % 7
	 e2 ~  | % 8
	 e4  b' | % 9
	 b  b | % 10
	 d  d | % 11
	 cis  cis8(  a) | % 12
	 b2 | % 13
	 b | % 14
	 fis4.  fis8 | % 15
	 g4  fis | % 16
	 e2 | % 17
	 e 
	\repeat volta 2 {
	 b'  ^\markup {\upright  "Kehrvers"} | % 19
	 a8.  a16  d8  a | % 20
	 b2 | % 21
	 a8.  a16  d8  a | % 22
	 b2 | % 23
	 a8.  g16  fis8  g | % 24
	 e2 
	}
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Hier   wächst   kein   A -- horn,   hier   wächst   kein   Pflau  
	 -- men -- baum.   Hier   wach -- sen   kei -- ne   Mäd -- chen  
	   -- her -- zen,   kei -- ne   Mäd -- chen -- her -- zen.  
	 La -- la -- ler -- al -- la   la   la -- ler -- al  
	 -- la   la   la -- ler -- al -- la   la.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}