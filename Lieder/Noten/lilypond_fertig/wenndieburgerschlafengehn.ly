\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  e2:m b:7 c4 d g2 a:m e:m b:7 b:7 e:m b:7 c4 d g2 a:m e:m b:7 b:7
}

chordNamesz = \chordmode {
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  b:7 a b:7 e e4 c a2 b:7 e e:7 a b:7 e e b:7 b:7 fis fis g g g g d c b:7 b:7 a b:7 e e a b:7 e
}

mel = \relative c' {
  \global
  e8 e g g |
  fis fis b,4 |
  e8 e d d |
  b b( b) r |
  c c d d |
  b b g'4 |
  fis8 e dis e |
  fis fis r4 |
  e8 e g g |
  fis fis b,4 |
  e8. e16 d8 c |
  b b r4 |
  c8. c16 d4 |
  e8. e16 g4 |
  fis8. e16 dis8 e |
  fis fis r4 \bar "||" \break
}  

melz = \relative c'' {
  \clef treble
  \time 2/4
  \key e \major
  r4 ^\markup {\upright  "Kehrvers"} b, |
  cis4. fis8 |
  e dis cis dis |
  e4 b |
  b c |
  cis4. fis8 |
  e dis cis dis |
  e2 |
  r4 b |
  cis4. fis8 |
  e dis cis dis |
  e4 b |
  b8 e dis e |
  fis4. fis8 |
  fis8. e16 dis8 e |
  fis4 r | \break
  r fis |
  gis4. fis8 |
  e dis e fis |
  gis4 fis |
  e dis |
  a'4. gis8 |
  fis e dis cis |
  b4 r |
  r b |
  cis4. fis8 |
  e dis cis dis |
  e4 b |
  gis'8. gis16 fis8 gis |
  a4 fis |
  e dis |
  e r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Wenn die Bür -- ger schla -- fen gehn, in der Zip -- fel -- müt -- ze,
  und zu ihr -- em Kö -- nig flehn, dass er sie be -- schüt -- ze,
  ziehn wir fest -- lich an -- ge -- tan hin zu den Ta -- ver -- nen.
  Schlen -- dri -- an, Schlen -- dri -- an, un -- ter den La -- ter -- nen.
}

versz = \lyricmode {
  Die Nacht ist nicht al -- lein zum Schla -- fen da, die Nacht ist da, dass was ge -- scheh',
  ein Schiff ist nicht nur für den Ha -- fen da, es muss hi -- naus, hi -- naus auf ho -- he See!
  Be -- rauscht euch, Freun -- de, trinkt und liebt und lacht, und lebt den schön -- sten Au -- gen -- blick!
  Die Nacht, die man in ei -- nem Rausch ver -- bracht be -- deu -- tet Se -- lig -- keit und Glück.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff
      \new Voice <<
	\mel
      >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
  
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNamesz}
      \new Staff
      \new Voice <<
	\melz
      >>
      \addlyrics {\set fontSize = #+2 \versz}
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






