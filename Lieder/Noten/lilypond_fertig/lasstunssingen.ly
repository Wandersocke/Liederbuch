\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 s1 a:m c g a:m d2:m a4:m e a1:m f a:m g a:m f a:m g a2:m
}

melody = \relative c' {
  \global
	 s2 s4 s8 e8 | % 2
	 e  a  a  a  a4  b8  a | % 3
	 g4  e8  e  e4  g8  g | % 4
	 b4  a8  a  g4  a8  b | % 5
	 a8.  b16  a8  g  e4  e8  e | % 6
	 f4  e8  d  c4  b8  b | % 7
	 a2 \bar "||" \break r4 ^\markup {\upright  "Kehrvers"} e'8  e | % 8
	 f4  a2  f8  f | % 9
	 e  c  a2  a8  a | % 10
	 d4  d  d8  d  c  b | % 11
	 c  b  a2  e'8  e | % 12
	 f4  a2  f8  f | % 13
	 e  c  a2  a8  a | % 14
	 d4  d  d8  d  c  b | % 15
	 a2 
	\bar "|."	
}

vers = \lyricmode{
	 \set stanza = #"1. " Und   als   wir   dann   am   A -- bend   den   See  
	 vor   uns   sahn,   fühl -- ten   wir   den   Be -- ginn  
	 uns -- res   A -- ben -- teu -- ers   nahn,   und   wir  
	 stie -- gen   zum   U -- fer   hin -- ab.   Lasst   uns  
	 sin -- gen,   lasst   uns _ sing -- en,   lasst   die  
	 Lie -- der   uns -- rer   Fahrt   er -- klin -- gen! _
	 Lasst   uns   zie -- hen,   lasst   uns _ zie      -hen,  
	 lasst   uns   wei -- ter   durch   die   Wäl -- der   ziehn!  
	
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
