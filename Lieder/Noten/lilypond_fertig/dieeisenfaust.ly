\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 e2:m d c4 d e2:m c d e:m e1:m d2 c4 d e2:m c d e1:m
  g2 d g4 d d2 e:m d e:m
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  e e fis fis |
  g8( a) g( fis) e4. b8 |
  e4. g8 fis( e) d4 |
  e e r b |
  e e fis fis |
  g8( a) g( fis) e4. b8 |
  e4. g8( fis) g d4 |
  e e r2 | \bar "||"
  \repeat volta 2 {
    b'2 a |
    b8( a) g4 fis4. fis8 |
    e4. g8( fis) e d4 |
    e e r 
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Die Ei -- sen -- faust am Lan -- zen -- schaft,
  die Zü -- gel in der Lin -- ken, so sprengt des Rei -- ches Rit -- 
  ter -- schaft, und ih -- re Schwer -- ter blin -- ken.
  Hej, hej, hej, he -- ja, und ih -- re Schwer -- ter blin -- ken.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






