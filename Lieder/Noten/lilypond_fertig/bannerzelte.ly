\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  a1:m a:m f f g g a:m e c c g g a:m a:m e e
  c c g g a:m a:m e e a:m a:m a:m f f g g a:m e a:m c g a:m a:m a:m a:m
  f f g g a:m e a:m a:m f g2 a:m e1
}

mel = \relative c' {
  \global
  a2. b4 |
  c b8 a4. g4 |
  f2. g4 |
  a a8 b4. c4 |
  c b8 b( b2) |
  b4 b8 c4. d4 |
  f e8 e( e2)( |
  e) r4 e |
  e2. f4 |
  g g8 f4. e4 |
  d2. d4 |
  f4 f8 e4. d4 |
  c2. a4 |
  e'4 e8 d4. c4 |
  b1 |
  r2. e4 |
  e2. f4 |
  g g8 f4. e4 |
  d2. d4 |
  f f8 e4. d4 |
  c2. a4 |
  e' e8 d4. c4 |
  b1 |
  r4 b c8 b4. |
  a2. r4 |
  e' e8 e4. d4 |
  c b8 a4. a4 |
  a' a8 a4. g4 |
  
  f e8 d4. d4 |
  g g8 g4. a4 |
  b b8 c4. b4 |
  a e8 e4. f4 |
  e f8 e4. f4 |
  e2 r4 e8 e |
  e2. f4 |
  g2. f4 |
  e1( |
  e2) r4 e |
  e e8 e4. d4 |
  c b8 a4. a4 |
  a'4 a8 a4. g4 |
  f e8 d4. d4 |
  g g8 g4. a4 |
  b b8 c4. b4 |
  g e8 e4. f4 |
  e f8 e4. f4 |
  e2. r4 |
  \bar "||" \break
  r1 ^\markup {\upright  "Zwischenspiel"} |
  r |
  r2 r |
  r1 |
  \bar ".|"
}

vers = \lyricmode{
  \set stanza = #"1. " Ban -- ner, Zel -- te, ''Wer "da?'' -" Ru -- fe, Stil -- le um das La -- ger her,
  Feu -- er schei -- nen in der Nacht. Im Man -- tel schläft die Wa -- che ein.
  Ein Leut -- nant schritt vor -- bei, das Wür -- fel -- spiel ist falsch.
  Aus ei -- nem Schat -- ten tritt her -- bei ein Spiel -- mann in den Kreis,
  der Pos -- ten lässt vor -- bei und flüs -- tert leis:
  Va -- ga -- bund, so hör mich an, die Nacht ist kurz und ir -- gend -- wann
  der Ruf des Kor -- nett laut er -- tönt, drum spie -- le mir das
  alt -- ge -- sung -- ne Lied. Und der Spiel -- mann singt ein Lied:
  Fünf Schwä -- ne durch die Öd -- mark ziehn,
  ein Kö -- nig mit vier Re -- cken hin, im Mor -- gen -- rot ihr
  Ban -- ner fliegt und wei -- ter geht es für das gu -- te Ziel.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}
