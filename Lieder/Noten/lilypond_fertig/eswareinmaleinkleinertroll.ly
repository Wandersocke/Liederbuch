\version "2.18.2"

%\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  s4 e1:m g d e:m e:m g d e:m g d c2 d g1 e:m g e2:m d e1:m b:m d a d a2 b:m7 e2:m
}

mel = \relative c' {
  \global
  \partial 4
  b4 |
  e4. e8 e4. e8 |
  b'4 b b4. b8 |
  a4 a g fis |
  e2. b4 |
  e4. e8 e4. e8 |
  b'4 b b4. b8 |
  e4 e d cis |
  b2. e,4 |
  b'4. b8 b4. b8 |
  d4 d d4. d8 |
  e4 e d c |
  d b8 b4. b4 |
  e,4. e8 e4. e8 |
  b'4 b b4. b8 |
  a4 a g fis |
  e e2 
  
   \break
  \repeat volta 2 {
    e4 |
    b'4. b8 b4 b |
    a g fis4. fis8 |
  }
  \alternative {
    { e4 fis g fis | a1 | }
    { e4 fis e d | e2. \bar "|." }
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Es war ein -- mal ein klei -- ner Troll, der hat -- te ganz viel Spaß.
  Er sang und sprang und hüpf -- te rum und wälz -- te sich im Gras.
  Er troll -- te auf der Wie -- se rum, in Pfüt -- zen macht er'n Pa -- pa nass,
  spielt Strei -- che, ko -- kelt und macht Dreck und al -- ke sa -- gen: Lass das!
  Doch klei -- ne Trol -- le tun nicht gern, was man zu ih -- nen sagt das den gan -- ten Tag.
}

verse = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  Sie sin -- gen, tan -- zen, sprin -- gen gern und
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
        \consists "Volta_engraver"
      } <<
        \new Voice = "first"
        { \mel }
        \addlyrics {\set fontSize = #+2 \vers}
        \addlyrics {\set fontSize = #+2 \verse}
      >>
    >>
    \midi{ }
    \layout {
      \context {
        \Score
        \remove "Volta_engraver"
      }
    }
  }
}