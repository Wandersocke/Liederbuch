\version "2.14.2"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key f \major
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  c4 f2 c c:7 f f b4 f c1 a2 d:m g c4 f
  c2 c4 g:7 c1 c g:m b f f b f2 c2 f
}

melody = \relative c' {
  \global
	 c4 | % 2
	 f4.  f8  e4  d | % 3
	 c  d8(  e)  f4  g | % 4
	 a4.  f8  bes4  a | % 5
	 g2 r4   g | % 6
	 a4.  g8  f4  f | % 7
	 g4.  f8  e4  a | % 8
	 g  g  a  c | % 9
	 c2 r4   c | % 10
	 g4.  g8  g4  a | % 11
	 bes4.  bes8  bes4  bes | % 12
	 f4.  f8  f4  g | % 13
	 a2 r4   a | % 14
	 c4.  c8  bes4  a | % 15
	 bes  c  d  d | % 16
	 c4.  a8  g4  c | % 17
	 f,2 r4  s4  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " All -- zeit   be -- reit!   Den   kur -- zen   Spruch  
	 als   Lo -- sung   ich   er -- kor;   ihn   schreib   ich  
	 in   mein   Le -- bens -- buch,   ihn   halt   ich   stets  
	 mir   vor.   Das   gibt   dem   Le -- ben   Zweck   und  
	 Ziel   gibt   Mut   und   Hei -- ter -- keit;   zu  
	 heil -- gem   Ernst,   zu   froh -- em   Spiel  
	 all -- zeit,   all -- zeit   be -- reit!  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
