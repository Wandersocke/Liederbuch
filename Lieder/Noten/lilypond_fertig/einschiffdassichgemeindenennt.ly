\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \set majorSevenSymbol = \markup { j7 }
  % Akkorde folgen hier.
  s4 d2:m c b4 b8 c4. c4 d2:m c d:m a:7
  d4:m c b2 a1:7 s g2:m c f1:7+ g2:m c f1:7+
  g2:m c f1:7+ d2:m e a1:7 s d2:m c b4 b8 c4. c4
  d2:m c d:m a:7 d4:m c b2 a1:7 g:m f:7+ g:m 
  a8:7 a4:7 d:m d:m d8:m 
  d2:m c b c f:7+ f8:7+ a4.:7 d1:m
}

mel = \relative c' {
  \global
	\repeat volta 2 {
	 a'4 | % 2
	 d,  d  e  e | % 3
	 f  f8  e4. r8   e 
	}
	
	\alternative {
	{
	 d4  d  c  c | % 5
	 d2. s4 \break
	}
	
	{
	| % 6
	 d  e  f  g | % 7
	 a2. s4  
	}}
	
	\bar "||"
	s2  s4   a | % 9
	 bes  bes  c  c | % 10
	 a  a8  f4. r8   f | % 11
	 bes4  bes  c  c | % 12
	 a2. r8   a | % 13
	 d4  d  e  e | % 14
	 c  c8  a4. r8   a | % 15
	 d4  d  c  c | % 16
	 a2. s4  
	\repeat volta 2 {
	s2  s4   a | % 18
	 d,  d  e  e | % 19
	 f  f8  e4. r8   e 
	}
	\alternative {
	{
	| % 20
	 d4  d  c  c | % 21
	 d2. s4  
	}
	
	{
	 d  e  f  g | % 23
	 a1 \break
	}}
	
	\bar "||"
	 d2 ^\markup {\upright  "Kehrvers"}  d | % 25
	 c8  a4  f8 ~   f2 | % 26
	 d'4(  f2)  d4 | % 27
	 c8  a4  f8 ~   f4  d8  e | % 28
	 f4  f8  f  g4  g8  g | % 29
	 a4  a8  a  g4.  a8 | % 30
	 a4  a  bes8  a4. | % 31
	 d1 
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Ein   Schiff,   das   sich   Ge -- mein -- de   nennt,   fährt  
	 durch   das   Meer   der   Zeit. " "" "" "" "" "             
	    Das   Schiff,   es   fährt   vom   Sturm   be -- droht  
	 durch   Angst,   Not   und   Ge -- fahr.   Ver -- zweif -- lung,  
	 Hoff -- nung,   Kampf   und   Sieg,   so   fährt   es   Jahr  
	 um   Jahr.   Und   im -- mer   wie -- der   fragt   man  
	 sich,   wird   denn   das   Schiff   be -- steh'n?        
	 " "" "" "" "" " Blei -- be   bei   uns   Herr,   blei  
	    -- be   bei   uns,   Herr,   denn   sonst   sind   wir  
	 al -- lein   auf   der   Fahrt   durch   das   Meer,   oh,  
	 blei -- be   bei   uns,   Herr!  
}
verse = \lyricmode{
	Das Ziel, das ihm die Rich -- tung weist, heißt 
	" "" "" "" "" "Got -- tes E -- wig 
	-- keit. " "" "" "" "" "" "" "" "
	" "" "" "" "" "" "" "" "" "
	" "" "" "" "" "" "" "" "" "
	" "" " Er -- reicht   es   wohl   das   gro -- ße  
	 Ziel,   wird  " "" "" "" "" "es nicht 
	un -- ter geh'n? " "" "" "" "" "" "
	" "" "" "" "" "" "" "" "" "
	" "" "" "" "" "" "" "" "" "
	" "" "" "" "" "
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
      
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
