\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 6/8
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  g4. d4. a4. d4. g4. d4. a2.
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    b'8.^\markup {\upright  "Kehrvers"}
    a16 g8 a g fis |
    e fis g fis e d |
    b' a g <fis a> <e g> <d fis> |
    e2.
  }
}

vers = \lyricmode {
  A -- ber es hält mich hier auf die -- ser Welt mit dir, 
  zwi -- schen Ge -- flüs -- ter und Schrei.
}

verse = \lyricmode {
  E -- ben ver -- ge -- ben wir jetzt un -- ser Le -- ben hier
  und das nicht nur ne -- ben -- bei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






