\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s8 |
  e1:m d2 e:m e4:m d2 e1:m d2 e1:m d2 e1:m d2 e8:m
}

mel = \relative c' {
  \global
  \partial 8
  \repeat volta 2 {
    b16 b |
    b8 e e e |
    e g fis e |
    d4 b8 
  } \break
  r |
  e4 fis8 g |
  a g fis e16 fis |
  g8 fis e d |
  e4 fis8 g |
  a g fis e16 fis |
  g8 fis e4 |
  <e b'> <fis b>8 <g b> |
  <a b> <g b> <fis a> <e a>16 <fis a> |
  <g b>8 <fis a> <e b'> <d b'> |
  <e b'>4 <fis b>8 <g b> |
  <a b> <g b> <fis a> <e a>16 <fis a> |
  <g b>8 <fis a> <e b'> \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Di -- gue, ding don, don, ce sont les filles des for -- ges.
  Des for -- ges de Paim -- pont, di -- gue, ding don, dai -- ne,
  des for -- ges de Paim -- pont, di -- gue, ding don, don,
  des for -- ges de paim -- pont, di -- gue, ding don, dai -- ne,
  des for -- ges de Paim -- pont, di -- gue, ding don, don.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






