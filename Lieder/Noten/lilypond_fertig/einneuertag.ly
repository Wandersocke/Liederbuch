\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 g1 e:m a:m d g e:m a2:m d:7 g1
  b:m a:m d:7 g b:m a:m d:7 g2 s4
}

mel = \relative c' {
  \global
  \partial 4

  d4^\markup {\upright  "Kehrvers"} |
  g g8 g4. a4 |
  b2. c8 b |
  a4 e2 e8 g |
  fis4 g8 a4. d,4 |
  g g8 g4. a4 |
  b2. c8 b |
  a4 e g fis |
  g2 r | \bar "|."
  fis4 fis fis fis |
  e e8 e8( e2) |
  r4 fis fis8 g4 a8 |
  g2 d |
  fis4 fis fis fis |
  e e8 e8( e2) |
  r4 fis8 fis fis g4 a8 |
  g2 r4 \bar "||"
  
  
  
}

vers = \lyricmode {
  Ein neu -- er Tag be -- ginnt, und ich freu' mich, ja ich freu -- e mich.
  Ein neu -- er Tag be -- ginnt, und ich freu' mich, Herr, auf Dich.
  \set stanza = #"1. " Warst die gan -- ze Nacht mir nah, da -- für 
  will ich dan -- ken. Herr, jetzt bin ich für Dich da, die -- se Stun -- de ist Dein.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






