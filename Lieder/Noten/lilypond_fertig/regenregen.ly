\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  g2 d g d e:m b:7 e4:m d e2:m
  e:m c e:m c g d g d e:m c e:m c g d g
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    b'8^\markup {\upright  "Kehrvers"} 
    b b b a8. b16 c8 a |
    b b b b a8. b16 c4 |
    g8 g g g fis8. g16 a8 fis |
    e e fis d e e e4 |
  }
  e e c d |
  e e c d |
  g g a a |
  b b a2 |
  e4 e c d |
  e e c d |
  g g a a |
  b b b2 \bar "||"
}

vers = \lyricmode {
  Re -- gen, Re -- gen, nichts als Re -- gen
  und die Näs -- se ü -- ber -- all. Was -- ser fließt auf
  al -- len We -- gen, Wol -- ken sind um Berg und Tal.
  \set stanza = #"1. " Trop -- fen fal -- len auf die Haa -- re,
  Wind der peitscht uns ins Ge -- sicht:
  Re -- gen, Re -- gen \set stanza = #" - " tau -- send Jah -- re
  und kein biß -- chen Son -- nen -- licht.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






