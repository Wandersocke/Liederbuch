\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s2 d1 a:m d a:m d g c d a:m a:m g g f f a:m a2:m
}

mel = \relative c' {
  \global
  \partial 2
  s8 ^\markup {\upright  "Kehrvers"} e e g |
  fis4. e8 d4 fis |
  e e r8 e e g |
  fis4. e8 d4 fis |
  e2( e8) e e g |
  fis4. fis8 fis4 fis |
  g d r8 g g g |
  g4. g8 g4 g |
  fis2 fis4 d |
  e1( |
  e2) e4 d |
  d1( |
  d2) d4 b |
  c1( |
  c2) c4 g4 |
  a1( |
  a2) r8 \bar "|."
}

vers = \lyricmode {
 Vor den Ge -- fah -- ren war -- nen Schil -- der
 mit groß -- en Let -- tern flam -- mend rot.
 Bleib stehn Ma -- ri -- a Mag -- da -- le -- na,
 der Gang zum Brun -- nen wär dein Tod.
 Inch Al -- lah, Inch Al -- lah, Inch Al -- lah, Inch Al -- lah.
}


\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    }
  }
}
