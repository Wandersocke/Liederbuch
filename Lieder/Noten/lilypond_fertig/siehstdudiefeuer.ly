\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 4/4
  %\tempo 4=120
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m e:m d e:m e:m d e:m e:m e:m e:m d e:m e:m d e:m
  e:m e:m e:m d d e:m e2:m d e1:m
}

mel = \relative c' {
  \global
  \set midiInstrument = #"flute"
  e2 e4 e |
  b'2 a4 g |
  a2 g4 fis |
  e2 r |
  e e4 e |
  fis2 d4 fis |
  e2 e | 
  r r4 b |
  e2 e4 e |
  b'2 a4 g |
  a2 g4 fis |
  e2 r4 b |
  e2 e4 e |
  fis2 d4 fis |
  e2 e |
  \repeat volta 2 {
    r2^\markup {\upright  "Kehrvers"}
    <<
    {
      \voiceOne
      \set midiInstrument = #"clarinet"
      \tiny
      b'4 d |
      e2 e |
      s4 e4 e d |
      d2. s4 |
      s2 b4 d |
      e2 e |
      s4 e e d, |
      b'2. s4 
    }
    \new Voice {
      \voiceTwo
      \set midiInstrument = #"flute"
      b4 a |
      b2 e, |
      a4\rest b b a |
      fis2. a4\rest |
      b2\rest g4 fis |
      g( fis) e2 |
      a4\rest d, b d |
      e2. a4\rest
    }
    \new Voice {
      \voiceThree
      \set midiInstrument = #"pipe"
      \teeny
      s2 |
      b4( a) g2 |
      s2. a8 g |
      a2. s4 |
      s1 |
      s4 a b2 |
      s4 a g fis |
      g2. s4
    }
    >> 
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Siehst du die Feu -- er
  er -- glim -- men zur Nacht, siehst du am
  Him -- mel die Ster -- ne?
  Scho -- scho -- nen am Feu -- er, ein
  Krie -- ger hält Wacht, die Büf -- fel,
  sie stam -- pfen von fer -- ne.
  Hei -- ja, hei -- ja, der Ne -- bel zieht.
  Hei -- ja, hei -- ja, der Büf -- fel flieht.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






