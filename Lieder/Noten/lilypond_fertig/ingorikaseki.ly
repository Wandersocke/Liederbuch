\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 e1:m e:m a:m b2:7 e:m e1:m e:m a:m b2:7 e:m g c g e:m b1:7
  e:m a:m e:m b:7 e:m \set chordChanges = ##f e2:m
}

mel = \relative c' {
  \global
  \partial 4 
  b4 |
  e e8 fis g4 g8 a |
  b4 b8 g b4 b8 b |
  a4 fis8 g a4 a8 c |
  b a g fis e4. b8 |
  e4 e8 fis g4 fis8 e |
  b'4 g8 a b4 b8 b |
  a4 fis8 g a4 a8 c |
  b a g fis e4 r | \break
  \repeat volta 2 {
    b'2^\markup {\upright  "Kehrvers"} c4 c |
    b4( g) e2 |
    fis g4 a |
    b2 r |
    a g4 a |
    b( g) e b'8 b |
    b4 a g fis |
  }
  \alternative {
    {
      e1 |
    }
    {
      e2. \bar "|."
    }
  }
}

vers = \lyricmode {
  \set stanza = #"1. " In Go -- ri -- ka -- se -- ki, am Ran -- de
  der Stra -- ße liegt ein -- sam ein Kna -- be, der regt sich
  nim -- mehr -- mehr. Ge -- fähr -- li -- che Stra -- ße, un -- heim
  -- li -- che Wei -- te, da -- rü -- ber die Wol -- ken sind wie
  ein Geis -- ter -- heer.
  Zwei -- tau -- send Rei -- ter fe -- dern her -- an,
  was sind da -- ge -- gen hun -- dert -- fünf -- und -- acht -- zig
  Mann. Mann.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






