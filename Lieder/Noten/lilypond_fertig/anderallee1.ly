\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 6/8
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1 d2  g4. d4. d1 d2 d2. g1 g2 a1 a2 d4. a4. g4. d4 a8 d2.
}

mel = \relative c' {
  \global
  d8. e16 fis8 fis e d |
  d8. e16 fis8 fis e d |
  b4. a4. |
  r2. |
  d8. e16 fis8 fis e d |
  d8. e16 fis8 fis e d |
  b2. |
  r2. |
  cis8. d16 e8 e d cis |
  cis8. d16 e8 e fis g |
  fis4. e4. |
  d8. cis16 b8 a d cis |
  d2. \bar "||"
}

vers = \lyricmode {
  \set stanza = #"1. " An der Al -- lee die A -- ka -- zi -- en blü -- hen, es schwe -- ben
  ü -- ber uns Flie -- ger und Vö -- gel, es ist wie -- der Mai.
  So könn -- ten wir uns -- re Zeit mit -- ein -- an -- der ver -- le -- ben,
  dach -- te ich heut ne -- ben -- bei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






