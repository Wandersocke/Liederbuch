\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  s1 d:m a b a:m d:m d4:m d8:m c2 c8 b1 a:m d:m 
  f f4 f8 e2:m e8:m a1 b d:m
}

melody = \relative c' {
  \global
	r2  r4 .  d8 | % 2
	 f4  d8  d4.  f4 | % 3
	 e  d8  e4.  e4 | % 4
	 f  d8  d4.  f4 | % 5
	 e  d8  e4.  e4 | % 6
	 f  f8  g4.  g8(  a) | % 7
	 a(  e)  d  e4.  e4 | % 8
	 f  d8  d4.  f4 | % 9
	 e4. (   f16  g  a4 )   e | % 10
	 d2 r4 .  d8 | % 11
	 f4  f8  g4.  g8(  a) | % 12
	 a(  bes)  a  e4.  e4 | % 13
	 cis  cis8  cis4.  cis4 | % 14
	 d2.  bes4 | % 15
	 a2 r2  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " In   mei -- nen   jun -- gen   Jah -- ren   trieb   mich  
	 Sehn -- sucht   oft   an   ei -- nen   Ort,   der   mich  
	 ge -- bannt   hielt wie  ein   Hort,   so  
	 war   die   Ein -- sam -- keit  mir  
	 lieb   von   ei -- nem   See,   an des --   
	 sen   Rand   ein   schwar -- zes   Fels -- ge -- mäu -- er  
	 stand.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \set chordNameLowercaseMinor = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
