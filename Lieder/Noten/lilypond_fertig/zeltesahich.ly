\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  c1 c c g2 c f1 f2 f4 g c1 g2 c
}

mel = \relative c' {
  \global
  g'4. g8 fis4 g |
  fis4 g a g |
  e4. fis8 g4 a |
  g f e2 |
  \repeat volta 2 {
    a4. b8 c4 b |
    a4. b8 a4 g |
    e4. f8 g4 a |
    g f e2 |
  }  
	
}

vers = \lyricmode {
  \set stanza = #"1. " Zel -- te sah ich, Pfer -- de, Fah -- nen,
  ro -- ter Rausch am Ho -- ri -- zont.
  Die mit uns ins La -- ger ka -- men, sind das Le -- ben so ge -- wohnt.
}


\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
