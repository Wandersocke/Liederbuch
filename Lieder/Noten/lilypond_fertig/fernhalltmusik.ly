\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s8 a1:m a:m e:m e:m d:m d:m e e a:m f e a:m a:m f e a2:m
}

mel = \relative c' {
  \global
  \partial 8
  e8 |
  e8. e16 e8 e e8. e16 f8 e |
  e2( e8) e c d |
  e4. e8 e e g f |
  e4 e4. e8 d e |
  f f4. f8 f g8. f16 |
  f2(f8) f f f |
  e8. e16 e8 e gis4 a |
  b8. b16 \bar "||" \break 
  b8 ^\markup {\upright  "Kehrvers"} b c4. b8 |
  b8. a16 a8 a a4. b8 |
  c8. c16 c8 c c4. c8 |
  b8. b16 b8 b c4 b |
  a4 a4. c8 c b |
  a4.(a4) c8 c b |
  c4.(c4) c8 c c |
  c4 b a8 gis fis gis |
  a2. r8 \bar "|."	
}

verse = \lyricmode {
  \set stanza = #"1. " Fern hallt Mu -- sik, doch hier ist stil -- le Nacht,
  mit Schlum -- mer -- duft an -- hau -- chen mich die Pflan -- zen,
  ich ha -- be im -- mer, im -- mer dein ge -- dacht,
  ich möch -- te schla -- fen, a -- ber du mußt tan -- zen,
  a -- ber du mußt tan -- zen, a -- ber du mußt tan -- zen,
  a -- ber du mußt tan -- zen, a -- ber du mußt tan -- zen,
  La -- la -- la lei, la -- la -- la lei, 
  la -- la -- la -- lei la la -- la -- la -- la lei.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\mel}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}

