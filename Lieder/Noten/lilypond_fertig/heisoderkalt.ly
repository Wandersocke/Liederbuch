\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 d1:m a2:7 d:m f1 c2 f d1:m g2:m a:7 d:m g:m f a:7 d4:m a:7 d2:m
}

melody = \relative c' {
  \global
	 d4  cis  d  d8.  e16 | % 2
	 f4  e8.  e16  d2 | % 3
	 f4  e  f  f8.  g16 | % 4
	 a4  g8.  g16  f2 
	\repeat volta 2 {
	 d4 ^\markup {\upright  "Kehrvers"} d8.  f16  a2 | % 6
	 bes4  a8.  g16  a2 | % 7
	 d4  a  bes  g8.  g16 
	}
	\alternative {
	{
	| % 8
	 f4  g  a2 
	}
	
	{
	 f4  e  d2 
	\bar "|."
	
	}
	}

}

vers = \lyricmode{
	\set stanza = #"1. " Heiß das Blut, das die A -- dern durch -- rauscht. 
	 Kalt   der   Wind,   der   das   Fah -- nen -- tuch   bauscht.  
	 Heiß   o -- der   Kalt!   Ja   o -- der   nein!   Nie  
	 -- mals   dür -- fen   wir   lau -- warm   sein!   lau -- warm  
	 sein!  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver"
    }{\melody}
     
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}

