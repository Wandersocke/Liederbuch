\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  s4 e2:m g a:m e:m d a:m e:m b:7 e:m g a:m e:m d a:m e:m
}

mel = \relative c' {
  \global
  \partial 4
  a8 ^\markup {\upright  "1"} a |
  e'4 e8 fis g4 fis8 g |
  e2. e8 e | 
  a4 a8 b c4 b8 a | \break
  b2. \fermata b8 ^\markup {\upright  "2"} a |
  g4 g8 a b4 c8 b |
  a c b a g4 g |
  fis fis8 g a4 g8 fis |
  g fis e4 \fermata r4 \bar "|."
}

vers = \lyricmode {
  Got -- tes Wort ist wie Licht in der Nacht; es hat Hoff -- nung und Zu -- kunft ge -- bracht,
  es gibt Trost, es gibt Halt in Be -- dräng -- nis, Not und Ängs -- ten, 
  ist wie ein Stern in der Dun -- kel -- heit.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






