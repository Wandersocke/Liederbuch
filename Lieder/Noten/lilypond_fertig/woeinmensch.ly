\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 c2 e:m a1:m f g c4 e:m a2:m f g c4 e:m a:m a:m7 f g c2
}

mel = \relative c' {
  \global
  c4 c e g |
  a e8 e( e4) r |
  f4 f a f |
  d c8 d( d4) r \bar "||" |
\break
  
  \repeat volta 2 {
   e4 ^\markup {\upright  "Kehrvers"} g c, c |
   f a d, d |
   e g c, c |
   c b c r |
  }
}


vers = \lyricmode{
\set stanza = #"1. " Wo ein Mensch Ver -- trau -- en gibt, nicht nur an sich sel -- ber denkt,
fällt ein Trop -- fen von dem Re -- gen, der aus Wüs -- ten Gär -- ten macht.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
