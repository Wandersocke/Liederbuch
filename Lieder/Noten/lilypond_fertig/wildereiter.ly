\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d2 a d a g d a a d a d a g d a d d d d:7 g a:7 d a d
}

mel = \relative c' {
  \global
  fis8. g16 a8 fis | 
  e fis g e |
  d e fis d |
  cis d e cis |
  b cis d b |
  a d cis d |
  e2 |
  e |
  fis8 g a fis |
  e fis g e |
  d e fis d |
  cis d e cis |
  b8. cis16 d8 b |
  a d cis d |
  e2 |
  d \bar "||" | \break
  \repeat volta 2 {
    r8 ^\markup {\upright  "Kehrvers"}  a d e |
    fis4 fis8 e |
    d d e fis | \break
    g4 g8 fis |
    e4 a8 g |
    fis fis e d |
    e4 d8 cis |
    d2
  }
}

vers = \lyricmode{
 \set stanza = #"1. " Wil -- de Rei -- ter, im -- mer wei -- ter auf der gro -- ßen Stra -- ßen -- lei -- ter 
 ja -- gen wir durch Tag und Nacht zum Mee -- re.
 In Er -- in -- ner -- ung der Zei -- ten, da der Buch -- ten stol -- ze Wei -- ten 
 uns be -- freit von al -- ler Er -- den -- schwe -- re.
 Je pense à vous, Made -- moi -- selle, je pense à vous
 Made -- moi -- selle, à la terre et à la mer, Cap Fré -- hel.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
