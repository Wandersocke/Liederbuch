\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  f2 g1:m f c f g:m f c f f2 c f d:m b c f1 b2 c d:m f b c f
}

mel = \relative c' {
  \global
  \partial 2
  r8 ^\markup {\upright  "Kehrvers"} c' bes a |
  g4 d' r8 bes a g |
  f4 c' r8 a g f |
  e4 f g c, |
  f g a8 c bes a |
  g4 d' r8 bes a g |
  f4 c' r8 a g f |
  e4 f g c, |
  f2. \bar "|." 
  \break c4 |
  f f g g |
  a a f f |
  d' d c g |
  a2. f4 |
  d' d c g |
  a a f c |
  d f g g |
  f2 \bar "||"
}

vers = \lyricmode {
  La -- la -- la -- la -- la, la -- la -- la -- la -- la, 
  la -- la -- la -- la -- la -- la -- la -- la -- la -- la, 
  la -- la -- la -- la -- la, la -- la -- la -- la -- la,
  la -- la -- la -- la -- la -- la -- la -- la.
  \set stanza = #"1. " Wenn ein -- ner sagt: "''Ich" mag dich, du; ich find dich wirk -- lich "gut!'',"
  dann krieg ich ei -- ne Gän -- se -- haut und auch ein biss -- chen Mut.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






