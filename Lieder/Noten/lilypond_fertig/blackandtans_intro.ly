\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef "treble_8"
  \time 4/4
}

mel = \relative c' {
  \global
  a8 b c4 b8 a e' d |
  c b a4. g8 a4 |
  e8 g a2. \bar "|."
}

\book {
  \score {
    <<

 \new Staff
    \new Voice <<
     \mel
   >>  
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






