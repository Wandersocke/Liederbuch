\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 c1 a:m f g c a:m f g
  c a:m f g c a:m f g c a:m f g c a:m f g c2
}

mel = \relative c' {
  \global
  \partial 4
  g8 g |
  c4 c r e8 c |
  a4 a r a8 c |
  f4 f r e8 f |
  d4 r2 g,4 |
  c c r e8 c | 
  a4 a r a8 c |
  f4 f r e8 f |
  g2 r \bar "||" |
  g2. ^\markup {\upright  "Kehrvers"} e8 g |
  a e4. r4 e |
  a2 g4 f |
  e8 d4. r4 e8 f |
  g2. e8 d |
  d c4. r4 a8 c |
  f2 e4 f |
  d d c b |
  c e8 e4 d8 e4 |
  r4 e8 e4 d8 e4 |
  r4 f8 f4 e8 f4 |
  d4 g a g |
  r4 e8 e4 d8 e4 |
  r4 e8 e4 d8 e4 |
  r4 f8 f4 e8 f4 |
  d4 d c b |
  c2 r4 s4|
    \bar "|."
	
}

vers = \lyricmode {
  \set stanza = #"1. " Durch die Fen -- ster des Ge -- fäng -- nis scheint die Son -- ne nicht he -- rein.
  Die Jah -- re, sie ver -- ge -- hen wie ein kur -- zer Au -- gen -- blick.
  Hell scheint die Son -- ne, er -- leuch -- tet den Him -- mel, 
  doch mein Herz das ist trau -- rig, weil ich nur an dich denk.
  Ich lie -- be dich! Ba -- dam, ba -- dam, ba -- dam, ba -- dam, ba -- dam, ba -- dam,
  bam, U -- hu -- hu! Ba -- dam, ba -- dam, ba -- dam, ba -- dam, ba -- dam, ba -- dam,
  bam. Ich lie -- be dich!
}


\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" }
        {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
