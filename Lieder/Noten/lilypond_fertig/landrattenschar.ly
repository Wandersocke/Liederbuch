\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  \set chordNameLowercaseMinor = ##t
  \override ChordName #'font-series = #'bold
  \germanChords
  b1:7 e:m c g b:7 e:m c b:7 e:m 
  e:m c g b:7 e:m c b:7 e:m
}

mel = \relative c'' {
  \global
  g2 fis |
  e8 e e e e4 e8 d |
  c c c d e4 c8 c |
  d d4 d8 d c4 c8 |
  b2. g'8 fis |
  e e4 d8 e4 e8 d |
  c c4 d8 d4 c8 c |
  b8 b4 b8 g' fis4 g8 |
  e4. r4 \bar "||"
  e8 ^\markup {\upright  "Kehrvers"} e d |
  e8 e4 d8 e e4 e8 |
  e8 e4 d8 e4 e8 e |
  d d4 d8 d c4 c8 |
  b2 r8 g' g fis |
  e e4 d8 e4 e8 d |
  c8 c4 d8 e4 c8 c |
  b8 b4 b8 g' fis4 g8 |
  e1 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Kei -- ne Schwie -- le an der Hand und das Tau noch un -- be -- kant, rührt der See -- gang noch in Bauch und Sinn.
  Sucht der Fuß stän -- dig Halt und der Wind scheint noch kalt, zieht die jun -- ge Crew erst -- mals da -- hin.
  So hisst die Se -- gel, oh e -- len -- de Land -- rat -- ten -- schar, denn der See -- gang ruft weit uns hi -- naus.
  Hal -- tet das Ru -- der auf Kurs und das Vor -- deck macht klar, wahrt den Kopf trotz Ge -- tos und Ge -- braus.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics { \set fontSize = #+2 \vers }
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






