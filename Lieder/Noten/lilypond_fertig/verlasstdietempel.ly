\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \time 4/4
  \partial 64*24
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  e2:m e4:m e8:m b2:7 e1:m g2:m d2 b1:7 a1:m e1:m e2:m b2:7
  e1:m e1:m e1:m b1:7 e1:m e1:m e1:m b1:7 e1:m
}

melody = \relative c' {
  \global
	 a'8  a  b | % 2
	 c4  c  b  b | % 3
	 a  a r8   a  a  a | % 4
	 g4.  g8  f4.  f8 | % 5
	 e2 r4   e | % 6
	 f4.  g8  a  a  g  f | % 7
	 e4  e r8   a  a  b | % 8
	 c4  c  b  b | % 9
	 a2 r4   a8  b | % 10
	 c4  a8  b  c4  a8  c | % 11
	 c4. (   b8  c )   c  c  d | % 12
	 e4  d  c  b | % 13
	 a2  a8 r8   a  b | % 14
	 c4  a8  b  c4  a8  c | % 15
	 e4. (   d8  e )   c  c  d | % 16
	 e4  d  c  b | % 17
	 a2  a8 s8  s4  
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Ver -- lasst   die   Tem -- pel   frem -- der   Göt -- ter,  
	 glaubt   nicht,   was   ihr   nicht   selbst   er -- kannt.   Das  
	 Schwert   will   Blut,   doch   Rost   frisst   Schwer -- ter,   und  
	 ü -- ber   al -- lem   herrscht   die   Hand   dei -- ne  
	 Hand,   dei -- ne   Hand   für   das   Land,        
	 und   sei -- ne   gros -- sen   Kam -- me -- ra -- den.  
	 Dei -- ne   Hand,   dei -- ne   Hand   für   das   Land,  
	       und   sei   ne   gros -- sen   Ka -- me  
	 -ra -- den.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \vers}
    >>
    \layout { }
  }
}