\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  a1:m a:m f a:m f c g g:7 c2 f f d:m g1:7 c g2:7 c1 f2 g:7 c1
}

melody = \relative c' {
  \global
	 a2  e'4.  c8 | % 2
	 a2  e'4  e | % 3
	 f2  g4  f | % 4
	 e2  e | % 5
	 f  g4  < f a > | % 6
	 < e g >2  < d f >4(  < c e >) | % 7
	 d2  g | % 8
	 g2. r4  | % 9
	 g  e8  d  c2 | % 10
	 c  a4  d8  d | % 11
	 b4  g2. | % 12
	 c4  g8  c  e4  d8(  c)  \time 6/4 | % 13
	 d4  f  e2 r2   \time 4/4  g(  a4)  f | % 15
	 e1 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Wir   sind   Ge -- fähr -- ten   des   rau -- schen -- den  
	 Win -- des,   der   durch   die   tie -- fen      Wäl  
	 -- der   geht,   frei   al -- ler   Fes -- seln,   fern   al  
	 -- ler   Bür -- de   ü -- ber   die   wei -- te     
	 Er -- de   weht,   Er -- de      weht.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}

