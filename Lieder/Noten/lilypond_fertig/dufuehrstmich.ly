\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key f \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 d1:m d:m f f bes2 c f4 c d2:m bes c f1
  f d:m c bes2:/d c:/e f1 d:m c bes2:/d c:/e f
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \partial 4
  d4 |
  d d8 e f4 f8 g |
  a4 a2 g4 |
  f f8( g) a4 a8 bes |
  c2. <f, c'>4 |
  \repeat volta 2 {
    <f d'> <f d'>8 <f d'> <e c'>4 <e c'>8 <e bes'> |
    <f a>4( <e g>) <d f>4. <d f>8 |
    <d f>4 <d g>8 <d a'> <e a>4 <e g>8 <f f> |
  }
  \alternative {
    {
      <c f>2. <f c'>4 |
    }
    {
      <c f>2 r8^\markup {\upright  "Kehrvers"} <f f> <a a> <c c> |
    }
  }
  <f, d'>4 <f c'> <f d'> <f c'> |
  <f a> <e g>2 r8 <e a> |
  <f bes>4. <f bes>8 <g c>4 <g c>8 <g bes> |
  <f a>2 r8 <f f> <a a> <c c> |
  <f, d'>4 <f c'> <f d'> <f c'> |
  <f a> <e g>2 r8 <e a> |
  <f bes>4 <f bes>8 <f c'> <e a>4 <e g>8 <f f> |
  <c f>2 r4 \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Du führst mich hin -- aus in die Wei -- te,
  du springst über Mau -- ern mit mir,
  stets weiß ich dich an mei -- ner Sei -- te, gib, dass ich den Weg nicht ver -- lier,
  stets lier. Ich will dir neu -- e Lie -- der sin -- gen, in Glück, in Freu -- de und Not,
  und will Ver -- zweif -- lung mich ver -- schlin -- gen,
  dann sing ich die Trau -- rig -- keit tot.
}

verse = \lyricmode {
  \set stanza = #"2. " Mit dir geht der Mut nicht ver -- lo -- ren,
  die Hoff -- nung schwin -- det mir nicht, du hast es den Men -- schen
  ge -- schwor -- ren, die Fin -- ster -- nis löscht nicht das Licht. Du Licht.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
          \addlyrics {\set fontSize = #+2 \verse}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






