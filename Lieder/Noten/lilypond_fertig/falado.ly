\version "2.14.2"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 c2 c4 g g2 c a4:m c1 c2 g g4 a:m a2:m g g4 a:m a2:m e e4 a2:m e4 a2:m
}

melody = \relative c' {
  \global
	 f8.  f16  f4  f | % 2
	 g8.  g16  g2 | % 3
	 a8.  a16  a4  bes | % 4
	 bes8.  a16  a2 | % 5
	 a8.  a16  c4.  a8 | % 6
	 a8.  g16  g2 | % 7
	 f8.  f16  f4  f | % 8
	 g8.  g16  g2 | % 9
	 f8.  f16  f2 | % 10
	 e8.  e16  e2 | % 11
	 d8  e  f  d  cis  e | % 12
	 d2 r4  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Fa -- la -- do,   o   Fa -- la -- do,   wer   sailt  
	 mit   nach   Fa -- la -- do?   Je -- der   sucht   es,  
	 kei -- ner   fand   Fa -- la -- do,   das   Wun -- der  
	 -- land.   Ein   Mast   ho,   zwei   Mast   ho,   Drei -- mast  
	 sailt   nach   Fa -- la -- do.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}

