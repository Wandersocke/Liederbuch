\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={ 
  \time 4/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 c1 a1:m f2 d2:m g1 c1 a2:m f2 e2:m d2:m 
  g2 g2
  \set chordChanges = ##f g1 
  \set chordChanges = ##t 
  c2 g2 d1:m f2 d2 g1 c2 e2:m f2 c2 f2 e2:m d2:m g2 
  f2 c2 d4:7 g4:7 c1
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 e8  f | % 2
	 g4  g  g  e8  d | % 3
	 c4  c  c4.  c8 | % 4
	 f4  g8  a  g4  f8  e | % 5
	 d2.  e8  f | % 6
	 g4  g  g  a8  b | % 7
	 c4  c  c4.  a8 | % 8
	 g4  e  d  c 
	}
	\alternative {
	{
	 d2. s4  
	}
	
	{
	| % 10
	 d1 \bar "||" |
	}}
	
	\repeat volta 2 {
	 e8 ^\markup {\upright  "Kehrvers"} e4  e8  g  g4  g8 | % 12
	 a2.  a8  a | % 13
	 a  a4  a8  a4  a | % 14
	 b  g2. | % 15
	 c4  c8  c  b  a g4  | % 16
	 a  a8  a  g  f  e4 
	}
	\alternative {
	{
	| % 17
	 a  a  g  e | % 18
	 c  c  d2 
	}
	
	{
	 a'4  a  g  c | % 20
	 c  b  c2 ~  | % 21
	 c2. s4  
	}}
	
	\bar "|."
	
}

vers = \lyricmode{
	  \set stanza = #"1. " Vie -- le     We -- ge     gibt     es     auf     die -- ser   
	  Welt,     doch     ein -- en     Weg     kann     man     nur     gehn.   
	  Und     die     Fra -- ge,     die     sich     mir     da -- durch   
	  stellt,     ist:     wel -- chen     Weg     ich     nehm.         _ Wei   
	  -se     mir     Herr,     dei -- nen     Weg,     dass     ich     wan   
	  -dle     in     dei -- ner     Wahr -- heit.     Lass     mich's     er   
	  -kenn -- en,   o   Herr,     und     be -- kenn -- en,     dass     du   
	  nie     ein -- en     Feh -- ler     machst.                  
	                    
}
verse = \lyricmode{
	  Soll     ich     den     Weg     geh -- hen,     der     mir     
	  ge -- fällt?     Such     ich     ein -- nen,     der     nicht     be -- quem?   
	  Doch     nicht     was     ich     wün -- sche     und     den -- ke   
	  zählt;     wie     Er     führt     will     ich    _      gehn!   " "
	" "" "" "" "" "" "" "" "" "
	" "" "" "" "" "" "  Und all mein 
	Sinn -- en und al -- les Be -- ginn -- en möcht " "
	" "" "" "" "" "" "ich dass du's Herr üb -- er -- wachst. 
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}