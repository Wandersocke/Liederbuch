\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key g \major
  \time 4/4
  %\tempo 4=140
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 e1:m d2 b:m c1 b2:m c g1 g b:m c c b:m c c e:m d2 c b:m c e:m d1 b:m e4.:m d4. d4 c1 e4.:m d4. d4 e:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \repeat volta 2 {
    \partial 4 
    g'8 a |
    b2. b8 b |
    \times 2/3 {a4 b a} \times 2/3 {a4 fis d} |
    e2. e8 e |
    \times 2/3 {d4 e fis} \times 2/3 {g4 fis e} |
    d2. r4 |
    r2 r4
  }
  g8 g | \break
  fis4. d4. b4 |
  e1( | 
  e2) r4 g8 g | 
  fis4. g4. a4 |
  e1( |
  e2) r4 g8 a |
  b4. b4. b4 |
  \time 2/4
  \times 2/3 {a4 b d} | 
  \time 4/4 
  g,4. g8 \times 2/3 {fis4 fis fis} |
  \times 2/3 {g4 g g } \times 2/3 {b4 a b} |
  a2. d,4 |
  b'2. d8 c |
  b4. a4. a4 |
  b2. d8 c |
  b4. a4. a4 |
  g1( |
  g2) r4 \bar "|."
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  \set stanza = #"1. " Es ist schön, wenn du spät im ver -- fin -- ster -- ten Raum ins ge -- glät -- te -- te Bett zu mir kriechst. 
  Dei -- ne Haut ist noch kühl, dei -- ne Hän -- de sind schwer; und dein Mund gibt sich zö -- gernd und tut bei al -- lem, 
  als ob es das ers -- te Mal wär, und das, lie -- be Lieb -- ste, ist gut. Lie -- be Lieb -- ste ist gut.
}

verse = \lyricmode {
  und mich an -- rührst mit dei -- nem kaum sicht -- ba -- ren Flaum und nach Sei -- fe und Pfef -- fer -- minz riechst.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new StaffGroup <<
        \new Staff {
      %    \clef "treble_8"
          \mel
          \addlyrics {\set fontSize = #+2 \vers}
          \addlyrics {\set fontSize = #+2 \verse}
        }
      %  \new TabStaff 
      %  {
      %    \tabChordRepeats \mel
      %  }
       
    >> 
  >> 
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






