\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key g \major 
  \time 3/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 e1:m e2:m b2:7 b4:7 e2:m e4:m a2:m a4:m
  e2:m e4:m a2:m a4:m b2:7 b4:7 e1:m e2:m b1:7 b2:7 g1 g1 g4
  d2 d4 e1:m e1:m e4:m b2:7 b4:7 e1:m e2:m b2:7 b4:7 e2:m
}

melody = \relative c' {
  \global
	 b4 | % 2
	 e2  e4 | % 3
	 g2  g4 | % 4
	 fis( e)  dis | % 5
	 e2  b4 | % 6
	 c(  b)  a | % 7
	 b2  b4 | % 8
	 c(  b)  a | % 9
	 b2  b4 | % 10
	 e2  fis4 | % 11
	 g2  a4 | % 12
	 b2. | % 13
	 b,4 r4   b' | % 14
	 b2  b4 | % 15
	 b2  b4 | % 16
	 b( a)  g | % 17
	 a2  a4 | % 18
	 g2  g4 | % 19
	 g2  g4 | % 20
	 g(  fis)  e | % 21
	 fis2  b,4 | % 22
	 e2. | % 23
	 e2  b4 | % 24
	 e2. | % 25
	 e2 s4  
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Die   ban -- ge   Nacht   ist   nun he -- rum,  
	 wir   fah -- ren   still,   wir   fah -- ren  
	 stumm,   wir   fah -- ren   ins   Ver -- der -- ben!   Wie  
	 weht   so   frisch   der   Mor -- gen -- wind,   gib  
	 her   noch   ei -- nen   Schluck      ge -- schwind   vorm  
	 Ster -- ben,   vorm   Ster -- ben.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}