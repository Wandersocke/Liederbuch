#(define ((chord-name->german-markup-text-alteration B-instead-of-Bb) pitch lowercase?)
 
   (define (pitch-alteration-semitones pitch)
    (inexact->exact (round (* (ly:pitch-alteration pitch) 2))))
    
   (define (conditional-string-downcase str condition)
    (if condition
        (string-downcase str)
        str))
      
   (let* ((name (ly:pitch-notename pitch))
          (alt-semitones  (pitch-alteration-semitones pitch))
          (n-a (if (member (cons name alt-semitones) `((6 . -1) (6 . -1)))
                   (cons 7 (+ (if B-instead-of-Bb 1 1) alt-semitones))
                   (cons name alt-semitones))))
     (make-line-markup
      (list
       (make-simple-markup
        (conditional-string-downcase
         (vector-ref #("C" "D" "E" "F" "G" "A" "H" "B") (car n-a))
        lowercase?))
       (let ((alteration (/ (cdr n-a) 2)))
         (cond
            ((and (equal? lowercase? #f) (= alteration FLAT) (= (car n-a) 7)) (make-simple-markup ""))
            ((and (= alteration FLAT) (or (= (car n-a) 5) (= (car n-a) 2) )) (make-simple-markup "s"))
            ((= alteration FLAT) (make-simple-markup "es"))
            ((and (= alteration DOUBLE-FLAT) (or (= (car n-a) 5)(= (car n-a) 2) )) (make-simple-markup "ses"))
            ((= alteration DOUBLE-FLAT) (make-simple-markup "eses"))
            ((= alteration SHARP) (make-simple-markup "is"))
            ((= alteration DOUBLE-SHARP) (make-simple-markup "isis"))
            (else empty-markup))))))) 
            
#(define germanChords (chord-name->german-markup-text-alteration #t))