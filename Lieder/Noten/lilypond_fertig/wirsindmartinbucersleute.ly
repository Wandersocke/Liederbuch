\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
  \tempo 4 = 130
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d4 g2 e:m d g g d g g c g
  d d e:m d g d g1 g e2 c d1 d g e2:m e4:m c
  g2 d g
}

melody = \relative c' {
   \global
   \set midiInstrument = #"flute"
   \partial 4
	 d4 | % 2
	 g \grace{  d8 [d8]} d4  e  e | % 3
	 d4.( c8)  b4  d | % 4
	\grace{  g8 [g8]} g4 \grace{  g8 [g8]} g4  a  d | % 5
	 b2.  b4 | % 6
	 c  c \grace{  c8[c8]} b4  \afterGrace b4 { b8[b8]} | % 7
	 a4( g)  fis  d | % 8
	 e  g8  g  fis4  a | % 9
	 g r4  r4   d | % 10
	 b4.  d8  g4  d | % 11
	 g4.  a8  b4  g | % 12
	 e  e  g  e | % 13
	 d2. \break
	\repeat volta 2 {
	 d4 | % 15
	 g  d  g  a | % 16
	 b4.  a8  g4  c | % 17
	 b \grace{  b8[b8]} b4  a  a 
	}
	\alternative {
	{
	 g2. s4  
	}
	
	{
	 g2. s4  
	\bar "|."
	
	}}
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Wir   sind   Martin   Buc -- ers   Leu -- te   und  
	 wollen   Kame -- ra -- den   sein;   als   Pfad -- finder   gestern  
	 und   heu -- te   stehn   wir   für   ein -- an -- der   ein.   
	 Und   bläst   der   Wind   uns   ins   Ge -- sicht,   gehts   ü -- ber   Stock   und   Stein:   
	 Wir   fürch -- ten   uns   noch   lan -- ge   nicht,   ziehn   fröh -- lich  
	 quer -- feld -- ein. -ein. 
}
mel = \relative c' {
      \global
      \set midiInstrument = #"clarinet"
      \partial 4
	 d4 | % 2
	 g \grace{  a8[a8]} a4  b  b | % 3
	 b(  a)  g  d | % 4
	\grace{  b8[b8]} b4 \grace{  b8[b8]} b4  d  fis | % 5
	 g2.  g4 | % 6
	 a  a \grace{  a8[a8]} g4  \afterGrace g4 { g8[g8]} | % 7
	 fis4( e)  d  d | % 8
	 c  g'8  g  d(  e)  fis4 | % 9
	 g r4  r4   d | % 10
	 g4.  a8  b4  g | % 11
	 e4.  fis8  g4  g | % 12
	 c  c  b  c | % 13
	 a2.
	\repeat volta 2 {
	 a4 | % 15
	 b  a  g  d | % 16
	 e8(  fis  g)  a  g4  c | % 17
	 d  d  c8(  d)  c4 
	}
	\alternative {
	{
	 b2. s4  
	}
	
	{
	 b2. s4  
	\bar "|."
	
	}}
	
}

\book {
  \score {
    << 
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
       }{\melody}
      \addlyrics {\set fontSize = #+2 \verse}
      \new Staff {\mel}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    
    \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
    
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
