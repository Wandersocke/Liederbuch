\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  a2:m c g a:m c g c1 f f8 a8:m a4:m a1:m a8:m c4 e:7 a4:m a8:m a2:m a1:m c g a:m
}

mel = \relative c' {
  \global
  a4 a8 b c2 |
  d8 d c b c a4. |
  c4 c8 c d d c d |
  e2 r |
  a8 a4. b8 c( c) b |
  a c a4. r |
  a8 a4. b8 c d b |
  g a4. r2 \bar "||" | \break
  \repeat volta 2 {
    a2 ^\markup {\upright  "Kehrvers"} a4 b |
    c2 b8( a4.) |
    b4. b8 a4 g |
    a2 r2 \bar "|." |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Ei -- si -- ger Wind, weh' mir in die Haa -- re,
  flieg' mei -- ne See -- le wie der Wind!
  Treib mich vor -- an auf der Stra -- ße!
  Va -- ter, nach Dir such' ich, Dein Kind.
  Bat Dich um Schu -- he, Flü -- gel gabst Du mir.
}
verse = \lyricmode {
  " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " 
  " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " " 
  Find' kei -- ne Ru -- he, bis ich ruh' in Dir.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\mel}
     
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}





