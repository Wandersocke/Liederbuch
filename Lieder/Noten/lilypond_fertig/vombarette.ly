\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  g1 g g d g2 a:m a1:m d g4 d g2 g1 e:m c a:m g g c2 d g1
}

mel = \relative c' {
  \global
  d4 d g g |
  b8( a) g( a) b4 g |
  g4. b8 a( g) fis( g) |
  a4 a a r |
  e e a a |
  c8( b) a( b) c4 a |
  d4. e8 d4. c8 |
  b4 a g r | \bar "||" \break
  \repeat volta 2 {
    g2 g |
    b4( a g fis)
    e2 e |
    a4( b c d) |
    c2 c |
    c4 d e c |
    d( fis e d)
    c2. r4
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Vom Ba -- ret -- te schwankt die Fe -- der, wiegt und biegt im Win -- de sich,
  un -- ser Wams von Büf -- fel -- le -- der ist zer -- fetzt von Hieb und Stich.
  Stich und Hieb und ein Lieb muss ein, muss ein Lands -- knecht ha -- ben.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






