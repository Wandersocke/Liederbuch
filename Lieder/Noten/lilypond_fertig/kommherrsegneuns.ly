\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
 g2 d e4:m d g2 a:m a:m7 d g
 d1:m a:m e2:m e:m7 a d g d4 g:7 c2 d4 g4 g2 d g1
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 g'2  d | % 2
	 g4  a  b2 | % 3
	 c4  b  a  g | % 4
	 a2  g 
	}
	 d'4.  d8  a4  b | % 6
	 c1 | % 7
	 b4  a  g  fis | % 8
	 e2  fis | % 9
	 g4.  g8  a4  b | % 10
	 c2  b | % 11
	 d4  b  a  a | % 12
	 g1 
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Komm,   Herr,   seg -- ne   uns,   dass   wir   uns   nicht  
	 tren -- nen,   Nie   sind   wir   al -- lein,   stets   sind  
	 wir   die   Dei -- nen,   lach -- en   o -- der   Wei  
	 -- nen   wird   ge -- seg -- net   sein.  
}
verse = \lyricmode{
	 son -- dern   ü -- ber -- all   uns   zu   Dir   be  
	 -- ken -- nen.                       
	                           
	                  
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {

    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
  }
}

