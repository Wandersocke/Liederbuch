\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s4 a1:m e e a:m a4 a2.:7 d1:m a2:m e a1:m e a:m 
  g c d:m a:m d2:m e:7 a1:7 d:m a:m d2:m e:7 a:m
}

mel = \relative c' {
  \global
  \partial 4
  e8 f |
  e4 a,8 b c a e' f |
  e4 b8 c d b e f |
  e4 b8 c d4 e8 f |
  e2. a8 a |
  a4 e8 f g e a bes |
  a4 d,8 e f a g f |
  a4 e8 c b4 d8 c |
  a2 r8 a a c |
  c4 b4. b8 b d |
  d4 c4. c8 c e |
  d2( d8) d e( f) |
  g2. \bar "||" \break
  e8^\markup {\upright  "Kehrvers"} g |
  g4 f2 d8 f |
  f4 e2 c8 e |
  d2 e |
  a2. e8 g |
  g4 f2 d8 f |
  f4 e2 c8 e |
  d2 e |
  a,2. \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Seht, die Leu -- te, sie sprin -- gen
  un -- ge -- schickt ü -- ber Pfüt -- zen, Was -- ser
  sam -- melt sich auf dem As -- phalt. Und sie 
  ge -- hen vo -- rü -- ber, Stund um Stund wird es
  trü -- ber, ja ich la -- che, mir ist gar nicht kalt.
  Un doch, ich spie -- le auf der Gar -- mosch -- ka, man sieht
  mich auf dem Trot -- toir. A -- ber lei -- der ist Ge -- burts -- tag
  nur ein -- mal im Jahr. A -- ber lei -- der ist Ge -- burts -- tag
  nur ein -- mal im Jahr.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






