\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a2:m f f8 g g4 a1:m f2 f8 g 
  g4 g2 a:m f f8 g g4 d1:m a2:m 
  a8:m e:7 e4:7 a:m
}

mel = \relative c' {
  \global
  <a a' c>4. r8 |
  <a' c>4. r8 |
  r8 <g b> <g b> <d g> |
  <e a>4. r8 | \break
  r8 <a, a' c> <a a' c> <a a' c> |
  <a' c>4 <a c> |
  r8 <g b> <g b> <d g> |
  <e a>4. r8 | \break
  r8 <a, a' c> <a a' c> <a a' c> |
  <a' c>4 <a c> |
  r4 <g  b>8 <a c> |
  <e a>4. r8 | \break
  r8 <d a> <e b> <f c> |
  <e a>4 <c e> |
  r8 <b e> <c c'> <b b'> |
  <a a'>4. r8 \bar "|." |
}

vers = \lyricmode {
  \set stanza = #"1. " Staub, Staub, und Step -- pen -- land,
  zwei al -- te Mu -- lis am We -- ges -- rand
  zie -- hen den Wa -- gen aus der Stadt,
  wei -- ter nach Os -- ten dreht sich das Rad.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






