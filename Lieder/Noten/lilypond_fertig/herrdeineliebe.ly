\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key d \major
  \time 4/4 
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  d1 e1:m a1:7 d1 b1:m e1:m a1:7 d1
  a1 d1 g1:6 fis1 b1:m e1:m g1:6 fis2 a2:7
  d1 e1:m a1:7 d1 b1:m e1:m a1:7 d
}

melody = \relative c' {
  \global
	 fis2  g4  a | % 2
	 g2.  fis4 | % 3
	 e  e  fis  g | % 4
	 fis  fis2 r4  | % 5
	 d2  e4  fis | % 6
	 e2.  d4 | % 7
	 cis  cis  d  e | % 8
	 d2. r4  | % 9
	 e2  e4  e | % 10
	 fis2.  fis4 | % 11
	 e  b'  g  e | % 12
	 fis  fis2 r4  | % 13
	 fis2  fis4  fis | % 14
	 g2.  fis4 | % 15
	 e  b'  g  e | % 16
	 fis2. r4 \bar "||" | % 17
	 fis2 ^\markup {\upright  "Kehrvers"} g4  a | % 18
	 g2.  fis4 | % 19
	 e  e  fis  g | % 20
	 fis  fis2 r4  | % 21
	 d2  e4  fis | % 22
	 e2.  d4 | % 23
	 cis  cis  d  e | % 24
	 d2. r4 \bar "|."
}

verse = \lyricmode{
	 \set stanza = #"1. " Herr,   dei -- ne   Lie -- be   ist   wie   Gras   und  
	 U -- fer,   wie   Wind   und   Wei -- te   und   wie  
	 ein   Zu -- haus.   Frei   sind   wir,   da   zu   woh  
	 -- nen   und   zu   ge -- hen.   Frei   sind   wir,   ja  
	 zu   sa -- gen   o -- der   nein.   Herr,   dei -- ne  
	 Lie -- be   ist   wie   Gras   und   U -- fer,   wie  
	 Wind   und   Wei -- te   und   wie   ein   Zu -- haus.  	
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}
