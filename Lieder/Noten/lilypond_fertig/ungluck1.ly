\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  g4. d8 e2:m g4. d8 c2 d1 e:m g c2 g a:m g d c e:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    g'4. fis8 e4 e |
    g4. fis8 e4 e |
    d2( d8) e fis g |
    e1 |
  }
  \repeat volta 2 {
    g4. a8 b4 b |
    c c b8( a) g4 |
  }
  a b8( c) d4 g, |
  g fis g2( |
  e) r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Un -- glück vor mir. Un -- glück nach mir, wo ich bin, ist Glück.
  Wo ich ging, blieb ei -- ne Fähr -- te um -- ge -- worf -- nen Lands zu -- rück.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






