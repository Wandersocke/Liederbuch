\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e2.:m g2. a2 b4:7 e2.:m a2.:m e2. d4:7 g2 b2. 
  c2. d2.:7 g2. c2.:7 e2.:m b2.:7 e2.:m
}

mel = \relative c' {
  \global
  e4. fis8 g a |
  b4 b2 |
  a4 g fis |
  e2 r4 |
  a2 a4 |
  g2 a4 |
  b b2( |
  b2.) |
  c4. c8 d c |
  b4 a2 |
  b4 a g |
  fis e fis |
  g2. |
  fis2. |
  e4 e2( |
  e2) r4 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Strah -- len brech -- en vie -- le aus ei -- nem Licht.
  Un -- ser Licht heißt Chris -- tus. Strah -- len brech -- en vie -- le aus 
  ei -- nem Licht, und wir sind eins durch Ihn.
  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






