\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 3/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
   s4 e1:m e2:m a2:m a4:m b2:7 b4:7 e2:m e4:m c2 c4 
   b1:7 b1:7 b4:7 a2:m a4:m d2:7 d4:7 g2 g4 c2 c4 a2:m 
   a4:m b2 b4 e2:m e4:m e2:7/e
}

melody = \relative c' {
  \global
	 b4 | % 2
	 e  e  e | % 3
	 g2  g4 | % 4
	 fis  fis  fis | % 5
	 dis2  b4 | % 6
	 e2  e4 | % 7
	 g  fis  g | % 8
	 b  b2 ~  | % 9
	 b s4  
	\repeat volta 2 {
	s2   b4 | % 11
	 c  c  b | % 12
	 a2  c4 | % 13
	 b ~   b  a | % 14
	 g2  g4 | % 15
	 fis  fis  fis | % 16
	 dis2  dis4 | % 17
	 e2. | % 18
	 e2 
	}
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Be -- wah -- re   uns,   Gott,   be -- hü -- te   uns,  
	 Gott,   sei   mit   uns   auf   un -- sern   We -- gen.  
	 Sei   Quel -- le   und   Brot   in   Wü -- sten -- not,  
	 sei   um   uns   mit   dei -- nem   Se -- gen.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \set chordNameLowercaseMinor = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
