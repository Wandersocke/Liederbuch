\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s2 s4. s8 a2:m f2 g2 a2:m c2 f2 g2 c2 c1 c8 f2. s8
  s8 c1 g2. s8 s1 a2:m g2 f1 c2 g2 g8 c8 c2 s8
}

mel = \relative c' {
  \global
  s2 s4. g8 |
  e'2 f4 e |
  d8 e4 f8 e4
  \autoBeamOff
  c8 c |
  g'4. g8 f4 e |
  d e8( f) e4. s8|
  \break
  c8 g'4 g8 g8 g4 g8 |
  g8 g4 c,2 r8 |
  g8 e' g4 g8 g a4 |
  g8 g2. r8 |
  \break
  s2. s8 g8 |
  c8 c4 c8 b b4 a8 |
  g4 c,2 r8 c8 |
  e8 f4 e8 d c4 b8 ( b8)
  c8 (c2) r8 s8
  \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Ihr sollt nicht im Dun -- kel ver -- mo -- dern, zu Bes -- se -- rem 
  seid ihr noch hier: Das Le -- ben will flam -- men und lo -- dern, und Freun -- de, das Le -- ben 
  seid ihr! Das Le -- ben will flam -- men und lo -- dern, und Freun -- de, das Le -- ben seid ihr! 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}
