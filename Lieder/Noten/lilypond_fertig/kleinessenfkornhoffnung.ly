\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1 b:m g2 e:m a1 d b:m e:m a fis b2:m g a1 d fis b2:m g a1 d
}

mel = \relative c' {
  \global
  fis4 fis a fis |
  d8 d4. r2 |
  g4 g b4. g8 |
  e2 r |
  fis4 fis a fis |
  d d r2 |
  e4 e e fis |
  e2 r |
  fis4 fis gis ais |
  b( a8) g4. r4 |
  a b a8 e4. |
  fis2 r |
  fis4 fis gis ais |
  b4 a8 g4. fis4 |
  e e d8 cis4. |
  d2 r \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Klei -- nes Senf -- korn Hoff -- nung,
  mir um -- sonst ge -- schenkt: Wer -- de ich dich pflan -- zen,
  dass du wei -- ter wächst, dass du wirst zum Bau -- me, der
  uns Schat -- ten wirft, Früch -- te trägt für al -- le, al -- le,
  die in Ängs -- ten sind.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






