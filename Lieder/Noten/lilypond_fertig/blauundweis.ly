\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  a1:m a1:m d1:m d1:m g1:7 c1 d1:m 
  \set chordChanges = ##f
  e1:7 e1:7 
  \set chordChanges = ##t
  a1:m a1:m d1:m g1:7 g1:7 c1 f1 f1 bes1 c1 f1 a1:m
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 a8.  a16  a8  b  c4  d | % 2
	 e1 | % 3
	 d8.  d16  d8  e  f4  g | % 4
	 a1 | % 5
	 g8.  g16  g8  a  g4  f | % 6
	 e1 | % 7
	 f8.  f16  f8  f  e4  d 
	}
	\alternative {
	{
	 b1 
	}
	
	{
	 b' 
	}}
	| % 10
	 c8 ^\markup {\upright  "Kehrvers"}  c  c  c  b4  a | % 11
	 c8.  c16  c8  c  b  a4  g8 | % 12
	 f1 | % 13
	 b8.  b16  b8  b  a4  g | % 14
	 b8.  b16  b8  b  a  g4  f8 | % 15
	 e1 | % 16
	 a8.  a16  a8  a  g4  f | % 17
	 a8.  a16  a8  a  a  g4  f8 | % 18
	 d1 | % 19
	 e8.  e16  e8  e  d4  c | % 20
	 b8  b  b  b  dis  e4  fis8 | % 21
	 e1 
	\bar "|."
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Als   ich   kam   letzt   von   zu   Haus,   mal -- te  
	 ich   im   Sinn   mir   aus,   was   ich   hier   be  
	 -rich -- ten   sollt',   auf   die   Fra -- gen   sa -- gen  
	 sollt'.      _ Blau   ist   un -- ser   Him -- mel,   li -- cht -- voll   sei -- ne   Au -- gen   blau.   Blau   si  
	 -- nd   uns -- re   Seen,   spie -- gelnd   in   des   Hi  
	 -- mm -- els   Au,   weiß   der   Schnee   und   leuch -- tend,  
	 Som -- mer -- nä -- chte   he -- ll   und   klar,   weiß  
	 die   Wol -- ken   zieh -- end,   blau -- en   Him -- mels  
	 Sch -- äf -- chen -- schar.  
}
verse = \lyricmode{
	 \set stanza = #"2. " Gäb'   von   Ar -- mut   ich   Be -- richt?   Wind -- fest  
	 sind   manch'   Tü -- ren   nicht!   Von   dem   Reich -- tum  
	 in   dem   Land?   End -- lich   ich   die   Ant -- wort  
	  _ fand:                       
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
