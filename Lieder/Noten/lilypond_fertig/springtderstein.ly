\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e2:m a:m e:m a:m c d g b:7 
  c g b1:7 e2:m b:7 e:m b:7
  e1:m a2:m e1:m b2:7 e1:m e2:m c d g4 e:m a2:m b:7 e1:m 
}

mel = \relative c' {
  \global
  e8. e16 e8 e e4 e8 fis |
  g4 e8 e e4 e8 fis |
  g4 fis8 g a4 g8 a |
  b4 b r2 |
  c4 c8 c b4 a8 g |
  a4 b8 b fis4 b8 a |
  g4 g8 e fis4 fis8 g |
  e8. e16 e8 fis b,4 \bar "||"
  cis8 dis | \break
  e2 b' | 
  c4 b8 a g4 b8 a |
  g4 g8 a fis4 fis8 g |
  e4 e r2 |
  e2 e' |
  d8 c b a g4  e8 g |
  b4 a g fis |
  e e r2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Springt der Stein vom Huf und der Re -- gen der rauscht und der Sturm
  ist dein treu -- er Be -- glei -- ter. Kei -- ner da -- bei, der mit ir -- gend -- wem tauscht,
  noch den Flö -- ten der fried -- li -- chen A -- bend -- stun -- de lauscht.
  Denn mit He -- ho geht un -- ser Ritt im -- mer wei -- ter und wei -- ter im Le -- ben.
  He -- ho, gehst du ein -- mal mit, musst du dich der Fahrt er -- ge -- ben.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        } <<
          \new Voice = "first"
            { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






