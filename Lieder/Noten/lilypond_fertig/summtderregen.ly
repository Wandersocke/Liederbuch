\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
  %\tempo 4=140
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e2.:m g2. d2. e2.:m e2.:m c2. b2.:7 e2.:m e2.:m g2. d2. e2.:m e2.:m c2. b2.:7 e2.:m
  g2. d2. g2. d2. g2. d2. g2. d2. e2.:m d2. c2. b2.:7 e2.:m c2. b2.:7 e2.:m
}

mel = \relative c' {
  \global
  b2 b4 |
  g'4. fis8 g4 |
  a4. g8 a4 |
  b2. |
  b,2 b4 |
  g'4. fis8 e4 |
  dis2. |
  e2. |
  b4. b8 b4 |
  g'4. fis8 g4 |
  a4. g8 a4 |
  b2. |
  b,2 b4 |
  g'4. fis8 e4 |
  dis2. |
  e2. |
  \repeat volta 2 {
    g2 g4 |
    a4. g8 a4 |
    b4. c8 b4 |
    a2 d,4 |
    g2 g4 |
    a4. g8 a4 |
    b2. |
    a2. |
    b4. c8 b4 |
    a4. g8 fis4 |
    g4. fis8 e4 |
    b'2. |
    b,2 b4 |
    g'4. fis8 e4 |
    dis2. |
    e2. |
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Summt der Re -- gen am A -- bend ins Tal, träumt mein Zelt von den Lom -- men.
  Ü -- ber den Strom. ü -- ber Klip -- pen und Rohr ist ihr Ru -- fen ge -- kom -- men.
  Jäh aus Wind wie ein hei -- se -- res Lied und schrill im pfei -- fen -- den We -- hen, ru -- dern
  die Lom -- men der Däm -- me -- rung zu, nacht -- wärts heim zu den Se -- en.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






