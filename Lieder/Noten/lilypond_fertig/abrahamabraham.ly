\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  e1:m a:m c b:7 e:m a:m c b2:7 e1:m d2 c b1:7 d2 c b:7 e:m d g1 c2 d e:m
}

mel = \relative c' {
  \global
  e4. ^\markup {\upright  "Kehrvers"} b'8 b2 |
  a4. e8 e4. fis8 |
  g4 e g4. a8 |
  b4 a b2 |
  e,4. b'8 b2 |
  a4. e8 e4. fis8 |
  g4 e g4. a8 |
  b4 b e,2 \bar "||"
  e4 b' b4. a8 |
  a4 g g fis |
  e b' b4. a8 |
  a4 g g fis |
  e fis g a |
  b b b2 |
  a8 b a g fis4 d |
  e2. r4 \bar "|."
}

vers = \lyricmode {
  A -- bra -- ham, A -- bra -- ham, ver -- lass dein Land und dei -- nen Stamm!
  A -- bra -- ham, A -- bra -- ham, ver -- lass dein Land und dei -- nen Stamm!
  \set stanza = #"1. " Mach dich auf die lan -- ge Rei -- se in ein Land, das ich dir wei -- se.
  Du sollst ge -- gen je -- den Schein Va -- ter dei -- nes Vol -- kes sein.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \vers}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






