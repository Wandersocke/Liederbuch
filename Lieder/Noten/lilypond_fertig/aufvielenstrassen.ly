\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s2 e1:m b:7 a:m e:m a:m e:m b:7 e2:m \set chordChanges = ##f e2:m
}

melody = \relative c' {
  \global
  \partial 2
  r8 b e fis | % 2
  g4.  e8  g4.  e8 | % 3
  fis2 r8   fis  fis  g | % 4
  a4  a  g  fis | % 5
  g8(  fis)  e4 
  \repeat volta 2 {
   r8 e8  g  b | % 7
   c4.  a8  c4.  a8 | % 8
   b2 r8   b  a  g | % 9
   fis2.  a4 | % 10
 }
  \alternative { {
   g8( a) b4
  }
  {
   g8(  fis)  e4 
   \bar "|."
} }
}

melo = \relative c' {
  \global
  \partial 2
  r8 b e dis |
  e2 e |
  dis4 cis b8 b dis e |
  c4 a b dis |
  e e 
  \repeat volta 2 {
    r2 |
    r8 a, c e fis a g fis |
    e4( fis g) e |
    dis e fis dis |
  }
  \alternative {{
    e4 e
  }
  {
    e4 e 
  }}
}

verse = \lyricmode{
	 \set stanza = #"1. " Auf   vie -- len   Stra -- ßen   die -- ser   Welt   habt  
	 ihr   euch   sorg -- los   rum -- ge -- trie -- ben,     
	 so   oh -- ne   Zelt   und   oh -- ne   Geld   der  
	 Tip -- pe -- lei   ver -- schrie -- ben. -- schrie -- ben.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
      \new Staff {\melo}
      \addlyrics {
	\set fontSize = #+2 \verse
      }
    >>
    \layout { }
  }
}
