\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  s2 g4 a4 b2.:m a4 d1 fis1:m b1:m g2. a4 d1 a1 e1:m fis1 fis1
  e1:m a1 b2:m a2 b2.:m a4 g1 b1:m a1 fis1 fis1 b1:m 
  a2 fis2 b1:m d2 a2 e1:m c1 a2 e4:m d4 e1:m a1:m
  e1:m a1:m e1:m d1 e1:m d2 a2 b1:m b1:m a1 b1:m
  e1:m a2 d2 e1:m a1:m g2 e2 a1:m c2 g2 
  d1:m bes2. d4:m g2:m d4:m c4 d2:m s2
  
}

mel =\transpose f d { 
  \relative c' {
  \global
  
  s2 f4 e4 | d4. d8 d4 e | f4 f f g | e4. e8 e4 c | d1 | f2. e4 |
  f4. f8 f4 g | e4 e f e | d4. d8 e4 d | e1 | r2 e4 f |
  g4. g8 g4 f | e e f e | d4. d8 e4 c | d r f e |
  d4. d8 d4 e | f f f f | g4. g8 g4 f | g1 | r | a4. a8 a4 f |
  g2 g | f4 d8 e f4 g | a a g r | g8. a16 bes8 a g4 f |
  g2 g4 g8 g | g4. g8 bes4 a | g2 r4 g8 g8 | g4. g8 c4. a8 |
  bes4 g r g8 g | g4. g8 c4. a8 | bes2 r4 bes8 bes | a4. a8 a4. a8 |
  bes8^\markup {\upright  "1.+2. Strophe"}( a) g4 r g8 g | f4. f8 g4 e | d2 r | f2 f4.( g8) | e2 c4.( d8) | d1
  \bar"|."
  \break
  bes'8^\markup {\upright  "3. Strophe"}( a) g4 r g8 g | g4. g8 bes4 a | g2 r | g4. g8 g4 e |
  f4 f f r | e c8( d) e4 f | g g f r |
  f8. g16 a8 g f4 e | f2 r4 f8 f | f4. f8 a4 g | f2 s2 \bar "|."  
  }
}

vers = \lyricmode {
  \set stanza = #"1. "Grau -- e Fel -- sen thro -- nen mäch -- tig ü -- ber tie -- fes lan -- ges Tal.
  Zieht der Fal -- ke durch die Stür -- me, streift sein Blick der Son -- ne Strahl.
  Ne -- bel liegt als fah -- ler Schlei -- er, fällt in herbst -- lich gold' -- nen Wald. 
  Spen -- det Trost der al -- ten Wei -- sen, der zu lei -- se wi -- der -- hallt.
  Und vom ho -- hen Fal -- ken auf ih -- rem Weg noch un -- er -- kannt, ist die Fahrt nicht, mehr zu hal -- ten,
  auf dem Weg durch frem -- des  Land.
  Sieh dich um und horch den Wei -- ten, die den Fal -- ken lang schon hör'n, lass dich von dem Geist nur lei -- ten,
  denn kein Mensch soll dies' Land stör'n, wo der Fal -- ke wohnt. klin -- gen, als die Fahrt zu wis -- sen glaubt:
  Wer den Ruf des Fal -- ken hört, sieht was sich an Fahr -- ten lohnt. Weiß, dass nichts den Frie -- den stört,
  wenn im Herz der Fal -- ke wohnt.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}