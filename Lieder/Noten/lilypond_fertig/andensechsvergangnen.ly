\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  a2:m d:m a4:m e a2:m 
  c2 f g c a:m d:m a4:m e a:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
   e4 e f e8( d) |
   c4 b c a
  }
 \break
  \repeat volta 2 {
   e' e a4. a8 |
   g4 f e2 |
   e4. f8 e4 d |
   c <b d> <a e'>2
  }
}

vers = \lyricmode{
 \set stanza = #"1. " An den sechs ver -- gang -- nen Ta -- gen
 we -- nig Freu -- de, Luft und Licht,
 Dreck an Hän -- den und Ge -- sicht.
}

verse = \lyricmode {
  muss -- ten wir uns lau -- sig pla -- gen,
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\mel}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
    }
    }
  }
}
