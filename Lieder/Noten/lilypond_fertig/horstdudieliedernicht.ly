\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  d1 d c:m d g:m f es c2:m d d1 g:m g:m d g:m f es d es es d
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    e2 b'4 b8( a) |
    gis4 a8 b( b2) |
    d,4. f8 a4 gis8( fis) |
    gis4 e8 e( e2) |
  } \break
  \repeat volta 2 {
    a8. a16 a8 b c4. b8 |
    d8. b16 b8 a b2 |
    a4 f8 f f2 |
    d4 e8 f e2 |
  }
  \repeat volta 2 {
    r8 ^\markup {\upright  "Kehrvers, erst nach 2. Strophe"}
    e' e e e8. c8. d8 |
    e1 |
    r8 e f e d8. c8. d8 |
    d8. c8. b8( b2) |
  }
  \repeat volta 2 {
    r8 <a c> <a c> <a c> <a c>8. <b d>8. <c e>8 |
    <b d> <a c> <g b>2 g4 |
  }
  \alternative {
    {
      r8 <f a> <f a> <f a> <f a>8. <g b>8. <f a>8 |
      <e gis>1 |
    }
    {
      f2. f4 |
      a2( g4) f |
      gis1 \bar "|."
    }
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Hörst du die Lie -- der nicht,
  die dein Seh -- nen dir ver -- spricht?
  Nennst du die -- se Ga -- be ein -- es Kör -- pers
  dein, schwing dein Bein, _ oh, schwing dein Bein.
  Wen weckt ein ein -- zi -- ges Lied aus tau -- send 
  -- jäh -- ri -- ger Mit -- ter -- nacht? Doch die -- ser
  Tanz un -- term Mai -- en -- baum _ bleibt für die
  E -- wig -- keit mein,
}

verse = \lyricmode {
  Dein dunk -- ler Traum ver -- flicht
  \set stanza = #"/:" in des Früh -- lings 
  An -- ge -- sicht._:/
  Ich weiß, den Kelch füllt nicht der Wein al -- lein,
  so wird die -- se Nacht mein Tanz _ sein.
  Der Zau -- ber, der uns ver -- riet, webt um uns
  al -- le mit al -- ler Macht. und von dem lan -- _ gen
  Win -- ter -- traum wirst _ _ _ _ _ _ _ du 
  ge -- ne -- sen sein.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
    
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >> 

   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}