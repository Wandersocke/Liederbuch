\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
  \autoBeamOff
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e1:m b:7 g2 d g1 c g d d:7 e2:m d c d 
  e1:m b:7 c2 d g b:7 e:m b:7 e:m
}

mel = \relative c' {
  \global
  \repeat volta 2 {
    g'4 e2 e8. g16 |
    fis2. fis8. fis16 |
    g4 g8 g a4 a8 [( a)] |
    b4 g2 g8( g) |
    c4 c8( c) b4 a |
    b b a g |
    fis4. fis8 fis4 g |
    a1 | \break
  }
  e8. ^\markup {\upright  "Kehrvers"}
  e8. e8 fis4 fis8 fis |
  g4 g fis fis8 fis | \break
  e8. e8. e8 e4 e |
  dis dis r2 |
  c8. c8. c8 d4 d8 d |
  g4 g fis fis8 fis |
  e8. e8. e8 e4 dis8 dis |
  e4 e r2 \bar "|." |
}

vers = \lyricmode {
  \set stanza = #"1. " Wei -- ter kommst du nicht, 
  als wo -- hin dei -- ne \set ignoreMelismata  = ##t Bei -- ne dich \unset ignoreMelismata tra -- gen, doch
  fol -- gen Ta -- ge Ta -- gen, setzt du wei -- ter 
  Schritt an Schritt. Lass sie doch re -- den, ihr Le -- ben lang, 
  die es im -- mer schon bes -- ser wis -- sen. Schaff dir dein
  Reich, drauß' am We -- ges -- rand, du wirst keins ihr -- er 
  Wor -- te ver -- mis -- sen.
}

verse = \lyricmode {
  Wei -- ter siehst du nicht, als wo -- hin dei -- ne 
  Au -- gen spä -- hen, \set ignoreMelismata  = ##t doch 
  du kannst ja die Ster -- ne \unset ignoreMelismata se -- hen
  und die Son -- ne im Ze -- nit.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






