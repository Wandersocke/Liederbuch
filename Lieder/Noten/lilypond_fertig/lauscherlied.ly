\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
  %\tempo 4 = 90
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  e2.:m g2. g2. d2. d2. d2. d2. e2.:m g2. g2.
  g2. g2. a2.:m b2.:m g2. g2. e2.:m b2.:m b2.:m
  g2. g2. c2. a2.:m a2.:m e2.:m e2.:m a2.:m a2.:m c2. b2.:m e2.:m e2.:m
}

mel = \relative c' {
  \voiceOne
  \global
  \set fontSize = #(magnification->font-size 0.75) {
  b'4 c d |
  d2 d4 |
  d e d |
  d2 d4 |
  d c b |
  d2 d4 |
  d d d |
  b2. |
  b2. |
  b2. |
  s2. |
  s2 d4 |
  e e d |
  c2 c8 c |
  s4 d d |
  d8 c4. b8 b |
  d4 c b |
  b2. |
  b2. |
  s2. |
  s2 d4 |
  e e d |
  c2. |
  c2. |
  s2. |
  s2. |
  s2. |
  s2. |
  e2. |
  d2 d4 |
  b2. |
  s2.
  }
}

mell = \relative c' {
  \voiceTwo
  \global
  \repeat volta 2 {
    \set melismaBusyProperties = #'()
        \slurDashed
  e4 g a |
  b2( b4) |
  b c b |
  a2( a4) |
  d, fis g |
  a2( a4) |
  a g fis |
  \slurSolid
  \unset melismaBusyProperties
  }
  \alternative{
   {
    e2. |
   }
   {
    g2.( |
    g2.) | \bar "||"
   }
  }
  
  d'4^\markup {\upright  "Kehrvers"} d d |
  d2 b4 |
  c c b |
  a2 g8( a) |
  b4 c b |
  b8 a4. g8 g |
  b4 a g |
  d2.( |
  d2.) |
  d'4 d d |
  d2 b4 |
  c c b |
  a2.( |
  a2.) |
  b4 c b |
  b2. |
  a4 b4. a8 |
  a2. |
  g2. |
  fis2 d4 |
  e2.( |
  e2.) |
  \bar "|."
}

melll = \relative c' {
  \voiceThree
  \global
  \set fontSize = #(magnification->font-size 0.75) {
    \override NoteColumn.force-hshift = #0.3
  s4 e b |
  g'2 g4 |
  g4 fis e |
  fis2 fis4 |
  fis e d |
  fis2 fis4 |
  fis e d |
  b2. |
  d2. |
  d2. |
  b'4 b b |
  b2 g4 |
  a4 a g |
  fis2 fis8 fis |
  g4 g g |
  g8 g4. s4 |
  e4 e e |
  fis2. |
  fis2. |
  b4 b b |
  b2 g4 |
  a a g |
  fis2. |
  fis2. |
  s2. |
  s2. |
  s2. |
  s2. |
  e2. |
  d2 s4 |
  b2. |
  b2. 
  }
}

vers = \lyricmode {
  \set stanza = #"1. " Lau -- scher, wo -- hin? _ Suchst du ein Ziel? Hörst du nur auf dich, _ hörst du nicht viel.
  _ Su -- che den Schim -- mer, su -- che den Glanz, du fin -- dest ihn nim -- mer, fin -- dest du es nicht ganz.
  Lau -- sche dem Klang und fol -- ge dem Ton, doch übst du Zwang, bringt mein Ge -- sang, dir bö -- sen Lohn.
}

verse = \lyricmode {
  Ein -- sam, ver -- sagt, ge -- bun -- den in Stein, _ so wirst du stehn. Wie lang noch al -- _ lein?
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff <<
    \new Voice = mellody \mell
    \new Voice = melody \mel
    \new Voice = melllody \melll
   >>
      \new Lyrics \lyricsto mellody { \set fontSize = #+2 \vers}
      \new Lyrics \lyricsto mellody { \set fontSize = #+2 \verse}
   >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
    \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
    }
    }
  }
}






