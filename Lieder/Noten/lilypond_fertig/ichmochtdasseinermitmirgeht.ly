\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
 s8 e2.:m b2.:7 e4.:m d4. g2. a4. d4. g4. g4 e8:m
  b4.:m e4:m d8 e4.:m b4.:m e2.:m a4.:m b4.:7 e4.:m g4. b2.:7 e4.:m a4.:m e4.:m e4:m
}

mel = \relative c' {
  \global
  \partial 8
  b8 |
  e4 e8 g4 g8 |
  fis4.( fis4) b,8 | 
  e4. r4  d8 | 
  g4 g8 b4 b8 |
  a4.( a4) d,8 |
  g4. r4  g8 | 
  fis4 fis8 g4 fis8 |
  e4. d4. |
  e4.( e4) d8 |
  c4. b4  b8 | 
  e4 e8 g4 g8 |
  fis4.( fis4) b,8 |
  e2.( |  e4.) r4 \bar ".|"
}

verse = \lyricmode {
 \set stanza = #"1. " Ich möcht', dass ei -- ner mit mir geht, der's Le -- ben kennt, der mich ver -- steht,
 der mich zu al -- len Zei -- ten kann ge -- lei -- ten. Ich möcht', dass ei -- ner mit mir geht.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
\addlyrics {\set fontSize = #+2 \verse}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






