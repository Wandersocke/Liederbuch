\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key e \major 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s4 e1 b:7 b:7 e b:7 e b:7 e a e b:7 e2 
  \set chordChanges = ##f
  e4
}

mel = \relative c' {
  \global
  \partial 4
  \repeat volta 2 {
    b4 |
    e8 e4 fis8 |
    gis4 e8 e |
    fis8 fis4 gis8 |
    a4 fis8 b |
    b4. a8 |
    gis4 fis |
    e2 ( |
    e4 )
  } \break
  b4 |
  dis2 |
  cis4 b |
  e2 |
  b |
  dis |
  cis4 b |
  e8. e16 e8 fis8 |
  gis2
  \repeat volta 2 {
    a2 |
    b4 cis |
    b2 |
    gis4 gis |
    b,2 |
    cis4 dis | 
  }
  \alternative {
    {
      e2
    }
    {
      e4
      \bar "|."
    }
  }
}

vers = \lyricmode{
  \set stanza = #"1. " Die Klamp -- fen er -- klin -- gen, wir wan -- dern und sin -- gen
  durchs Fran -- ken -- land zum Rhein. 
  Scho -- la -- ren auf Rei -- sen, fah -- ren -- de Leut, ja fahr'n -- de Leut'
  sin -- gen dann lei -- se: Wo blei -- ben wir heut? heut?
}

verse = \lyricmode {
  Die Bocks -- beu -- tel win -- ken
  und las -- sen sich trin -- ken, weil sie be -- zah -- let sein.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
	\consists "Volta_engraver" 
        }
        {\mel}
        \addlyrics {\set fontSize = #+2 \vers}
        \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
