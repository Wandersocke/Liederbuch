\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble 
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  c1 a:m f d:m g d:m g c c a:m f d2:m
  g1 d1:m g1 c1 c a:m f d:m g d:m g c2
}

melody = \relative c' {
  \global
	\repeat volta 2 {
	 e8  e4  e8 | % 2
	 d4  c | % 3
	 e8  e4  e8 | % 4
	 d4  c | % 5
	 f8  f4  f8 | % 6
	 f4  e | % 7
	 d2 | % 8
	r2  | % 9
	\break
	 d8  d4  d8 | % 10
	 c4  b | % 11
	 d8  d4  d8 | % 12
	 c4  b | % 13
	 e8  e4  e8 | % 14
	 e4  d | % 15
	 c2 | % 16
	r2  
	}
	\break
	 e2 ^\markup {\upright  "Kehrvers"}| % 18
	 e4  f | % 19
	 g4.  g8 | % 20
	 c4  b | % 21
	 a2 | % 22
	 a4  g | % 23
	 f2 | % 24 
	 \break
	 d4.  d8 | % 25
	 e4  d | % 26
	 f4.  f8 | % 27
	 b4  a | % 28
	 g4.  a8 | % 29
	 g4  f | % 30
	 e2 | % 31
	r2  | % 32
	\break 
	e2 | % 33
	 e4  f | % 34
	 g4.  g8 | % 35
	 c4  b | % 36
	 a2 | % 37
	 a4  g | % 38
	 f2 | % 39
	r2  | % 40
	\break 
	f8  f4  f8 | % 41
	 e4  d | % 42
	 f8  f4  f8 | % 43
	 e4  d | % 44
	 f8  f4  f8 | % 45
	 e4  d | % 46
	 c2 \bar "|." | % 47
	
}

vers = \lyricmode{
	 \set stanza = #"1. " Le -- ben   im   Schat -- ten,   Ster -- ben   auf   Ra  
	 -- ten,   ha -- ben   wir   was   da -- von?   Haß   und  
	 Em -- pör-   ung,   Leid   und   Ent -- behr -- ung,   ist  
	 das   die   End -- sta -- tion?   Gott   läd   uns   ein  
	 zu   sei -- nem   Fest,   laßt   uns   gehn   und   es  
	 al -- len   sa -- gen,   die   wir   auf   dem   We  
	 -- ge   sehn.   Gott   lädt   uns   ein,   das   hal -- tet  
	 fest,   wenn   wir   gehn.   Wo -- rauf   noch   war -- ten?  
	 Wa -- rum   nicht   star -- ten?   Lasst   al -- les   an  
	 -- dre   stehn.  
}
verse = \lyricmode{
	 Wäh -- rend   die   Fra -- gen   noch   an   uns   na  
	 -- gen,   kommt   ei -- ner   her   und   ruft:   Laßt   doch  
	 das   War -- ten,   laßt   es   euch   sa -- gen:   Freu  
	 -- de   liegt   in   der   Luft.  
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff \with {
      \consists "Volta_engraver"
    }{\melody}
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}
