\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key f \major 
  \time 6/4 
  \partial 64*16
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
 s4 f1 f2 c c4 f f2 b b4 f f2
 c c4 f f2 b b4 c1 c2 f f4 b b2 f f4
 c c2 f f4 b b2 f1 f4 c4 f2
}

melody = \relative c' {
  \global
	 c4 | % 2
	 f2  f4  f(  e)  f | % 3
	 g(  a)  bes  bes  a  f | % 4
	 d(  e)  f  c2  c4 | % 5
	 c(  d)  e  f2  c4 | % 6
	 bes'2  bes4  g2  g4 | % 7
	 g(  c)  bes  bes  a  f | % 8
	 d'2  d4  c2  a4 | % 9
	 g(  a)  bes  a2  f4 | % 10
	 bes2  bes4  a(  g)  f | % 11
	 f(  a)  g  f2 
	\bar "|."
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Der   A -- bend   füllt      die   gro -- ßen     
	 Wei -- ten   mit   sei -- ner      lei -- sen   Stim  
	 -- me      aus.   Die   ro -- ten   Wet -- ter -- wol  
	 -- ken      rei -- ten,   wir   geh -- en   spät   und  
	 still      nach   Haus,   wir   geh -- en   spät     
	 und   still      nach   Haus.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
