\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \key d \major
  \time 4/4 
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  % Akkorde folgen hier.
  d2 a b:m fis:m g d a1 d2 a b:m fis:m g a d1
  g2 a d a:7 d e:7 a1 d2 a b:m fis:m g a d
}

mel = \relative c' {
  \global
  fis4 fis8 a e4 e |
  d8 d d fis cis4 cis |
  b8 cis d g fis e d fis |
  e2 r |
  fis4 fis8 a e4 e |
  d d8 fis cis4 cis8 cis |
  b cis d g fis e d cis |
  d2 r | \bar "||"
  b4.^\markup {\upright  "Kehrvers"}  d8 cis4 e4 |
  d fis e g |
  fis8 fis fis fis fis e d fis |
  e2 r |
  fis4 fis8( a) e4 e |
  d d8( fis) cis4 cis |
  d8 cis d g fis e d cis |
  d2 r \bar "|."
}

vers = \lyricmode{
	 \set stanza = #"1. " Mö -- ge   die   Stra -- ße   uns   zu -- sam -- men -- 
	 füh -- ren   und   der   Wind   in   dei -- nem   Rück  
	 -- en   sein;   sanft  fal -- le  Re -- gen   auf   dei  
	 -- ne   Fel -- der   und   warm   auf   dein   Ge -- sicht  
	 der   Son -- nen -- schein.   Und   bis   wir   uns   wie  
	 -- der -- se -- hen   hal -- te   Gott   dich   fest   in  
	 Sei -- ner   Hand,   und   bis  wir   uns   wie --  
	 der --  se -- hen,   hal -- te   Gott   dich   fest  
	 in   Sei -- ner   Hand.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{\context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"      
    }} 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}
