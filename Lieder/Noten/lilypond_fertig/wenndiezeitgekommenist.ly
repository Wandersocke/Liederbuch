\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup=##f
  scoreTitleMarkup=##f
}

global={
  \key g \major
  \time 2/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  e2:m d2 e:m e:m d d e:m e:m d d g g e:m e:m d d e:m
  e4:m e8:m d e2:m e4:m e8:m d e2:m e:m d d e2:m
  e4:m e8:m d e2:m e4:m e8:m d e2:m e:m d d e2:m
}

melody = \relative c' {
  \global
  % Musik folgt hier.
  b2 | % 2
  d | % 3
  e8.  e16  e8  e | % 4
  e4  fis8  g | % 5
  fis8.  e16  e8  e | % 6
  e4  b8  d | % 7
  e8.  e16  e8  e | % 8
  e4  fis8  g | % 9
  a2 ~  | % 10
  a4  g8  a | % 11
  b4  b | % 12
  b  a8  g | % 13
  b4  b | % 14
  b  a8  g | % 15
  a4  a | % 16
  a8.  a16  g8  fis | % 17
  e2 ~ \bar "||" | % 18
  e8. ^\markup {\upright  "Kehrvers"} g16  g8  fis | % 19
  e2 ~  | % 20
  e8.  g16  g8  fis | % 21
  e2 ~  | % 22
  e8.  e16  e8  e | % 23
  d4  d | % 24
  d8.  d16  fis8  d | % 25
  e2 ~  | % 26
  e8.  < g b >16  < g b >8  < fis a > | % 27
  < e b' >2 ~  | % 28
  < e b' >8.  < g b >16  < g b >8  < fis a > | % 29
  < e b' >2 ~  | % 30
  < e b' >8.  < e b' >16  < e b' >8  < e b' > | % 31
  < d a' >4  < d a' > | % 32
  < d a' >8.  < d a' >16  fis8  < d a' > | % 33
  < e b' >2 ~  | % 34
  < e b' >4 r4 
  \bar "|." 
}

verse = \lyricmode {
  % Text folgt hier.
 \set stanza = #"1. " Wenn die Zeit ge -- kom -- men ist für ein schö -- nes klei -- nes Fest, 
 wenn die Zeit ge -- kom -- men ist für ein Lied,
 ja dann sin -- gen wir, ja dann spie -- len wir, 
 ja dann fei -- ern wir die gan -- ze Nacht.
 Tschi -- rim -- tim -- tim, tschi -- rim -- tim -- tim, 
 tschi -- rim -- tim tim tim tim tschi -- rim tim -- tim, 
 tschi -- rim -- tim -- tim, tschi -- rim -- tim -- tim, 
 tschi -- rim -- tim tim tim tim tschi -- rim tim -- tim. 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
