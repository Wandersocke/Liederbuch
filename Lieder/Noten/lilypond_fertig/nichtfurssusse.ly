\version "2.12.3"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key c \major
  \time 7/8
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  a4:m a8:m f2 c4 c8 g2
  a4:m a8:m f2 c4 c8 g2
  c4 c8 e2:m a4:m a8:m a2:m
  f4 f8 g2 e4:m e8:m a2:m
  a4:m a8:m f2 g4 g8 c2
  a4:m a8:m f2 g4 g8 a2:m
  f4 f8 g2 a4:m a8:m a2:m
  f4 f8 g2 e4:m e8:m a2:m
}

mel = \relative c' {
  \global
  e4 e8 f4 f |
  e e8 d4 d |
  e e8 f4 f |
  e e8 d2 |
  g4 g8 gis4 a8( b) |
  c4 b8 c4 c8( d) |
  c4 c8 b4 g |
  e g8 a2 |
  e4 e8 a4 a |
  g g8 e4 e |
  e e8 a4 a |
  g g8 e2 |
  c'4 c8 b4 g |
  a g8 a( b) c( d) |
  c4 c8 b4 g |
  e g8 a2 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Nicht fürs Sü -- ße,
  nur fürs Schar -- fe und fürs Bitt -- re
  bin ich da; schlag', ihr Leu -- te, nicht
  die Har -- fe, spiel' die Zieh -- har -- 
  mo -- ni -- ka.
  Lai la lai lai lai la lai lai lai la lai lai
  lai la la,
  schlag', ihr Leu -- te, nicht die Har -- fe, 
  spiel' die Zieh -- har -- mo -- ni -- ka.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






