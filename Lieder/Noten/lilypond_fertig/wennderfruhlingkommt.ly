\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \germanChords
  \set chordNameLowercaseMinor = ##t
  s4 e2:m b:m g e:m c1 d e2:m b:m g 
  e:m c1 d g d c b:7 c2 d:7 g1 g d c 
  b:7 e2:m b:7 e1:m e2:m b:7 e:m
}

mel = \relative c' {
  \global
  \partial 4
  g'8 fis |
  e4 e b b8 b |
  d4 d g, g8 g |
  c4 c8 c c4 d8 e |
  d4 d d g8 fis |
  e4 e8 e b4 b |
  d d8 d g,4 g8 g |
  c4 c c d8 e |
  d4 d8 d d4 \bar "||" g8 ^\markup {\upright  "Kehrvers"} a |
  b4 b b a8 g |
  a4 a8 a a4 g8 fis |
  g4 g8 g g4 fis8 e |
  fis4 fis8 fis fis4 e8 d |
  e4 e d8 d c c |
  b2 r4 g'8 a |
  b4 b8 b b4 a8 g |
  a4 a8 a a4 g8 fis |
  g4 g8 g g4 fis8 e |
  fis4 fis8 fis fis4 e8 fis |
  g4 e8 e b4 fis' |
  e4 e8 e e4 e8 fis |
  g4 e8 e b4 fis'8 fis |
  e2 r4 \bar "|."
}

vers = \lyricmode {
  \set stanza = #"1. " Wenn der Früh -- ling kommt 
  und die Vö -- gel ziehn und die düs -- te -- ren
  Wol -- ken nach Nor -- den fliehn, wenn man 
  Freu -- de und Freu -- heit den -- noch ver -- liert,
  weil der grau -- e All -- tag die Men -- schen re -- giert,
  ist es Zeit, die le -- der -- nen Ho -- sen zu
  tra -- gen, die al -- ten, ver -- wa -- sche -- nen
  Klamp -- fen zu schla -- gen und A -- ben -- teu -- er
  zu be -- stehn. Und es lohnt sich, die ur -- al -- ten 
  Lie -- der zu sin -- gen, durch Wäl -- der zu strei -- fen
  und Ber -- ge zu zwin -- gen, die ur -- al -- te Son -- ne
  wie -- der zu sehn und die ur -- al -- te Son -- ne zu sehn.
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
   
      \addlyrics {\set fontSize = #+2 \vers}
   >>
   \midi{ } 
   \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






