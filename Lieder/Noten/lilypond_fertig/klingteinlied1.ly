\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major
  \time 3/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  s2 s4 e:m e2:m d d4 g g2 d d4 e:m e2:m b:m b4:m
  c2 d4 e1:m e:m e4:m d d2 g g4 b1:7 b2:7 a1:m a:m a4:m 
  e1:m e4:m d4 g2 d4 g g2 b:7 b:7 b:7
  e2:m e4:m c c2 g g4 d d2 a:m a4:m e:m e2:m d d4 e:m e2:m
}

mel = \relative c' {
  \global
  r2 b8 b |
  e4. fis8 g4 |
  fis2 e8 d |
  g4 a b |
  a2 g8 a |
  b4. a8 g4 |
  fis g fis |
  e g fis |
  e2. |
  r2 b8 b |
  e4. fis8 g4 |
  fis4 e d |
  g a b |
  b2. |
  r2 a8 b |
  c4. b8 a4 |
  c b a |
  c b a |
  b e,2 |
  r2 d8 d |
  g4 g a |
  b2 a4 |
  b4. b8 b4 |
  b2. \bar "||" | \break
  \repeat volta 2 {
    b2. ^\markup {\upright  "Kehrvers"} |
    c2. |
    d4. c8 b4 |
    a2. |
    c2. |
    b2. |
    a4. g8 fis4 |
    e2. |
  }
}

verse = \lyricmode {
  \set stanza = #"1. " Klingt ein Lied durch die Nacht, klingt so schreck -- lich ver -- traut.
  Vol -- ler Sehn -- sucht, doch lüs -- tern, mord -- gie -- rig und roh.
  Al -- le Rat -- ten an Bord, eh der Mor -- gen uns graut.
  Ihr Ge -- such -- ten, Ver -- ruch -- ten, auf e -- wig Ver -- fluch -- ten.
  Und schon bald schallt es laut: Pi -- ra -- ten jo -- hoo!
  Jo -- hoo, grü -- ßet uns froh, jo -- hoo, fern Fa -- la -- do.
}

\book {
  \score {
    <<
      
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff
    \new Voice <<
     \mel
   >>
      \addlyrics {\set fontSize = #+2 \verse}
   >>
    \layout {
    \context {
      \Score
      \remove "Volta_engraver"
    }
    }
  }
}






