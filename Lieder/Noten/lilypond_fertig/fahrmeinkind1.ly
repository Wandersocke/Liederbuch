\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key d \major 
  \time 4/4
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  d1 a d d2 a2 a1 d a b:m a a d b:7 g d2 a d1 
}

melody = \relative c' {
  \global
	 fis4.  e8  d4  fis | % 2
	 e  d8  cis4.  e4 | % 3
	r4   d8  d  d4  d | % 4
	 fis  fis8  e(  e2 | % 5
	 e1) | % 6
	r4   fis8  e  d4  fis | % 7
	 e4.  d8  cis4  e | % 8
	r2   d4  d | % 9
	 fis2  e ~  | % 10
	 e1 
	\repeat volta 2 {
	r4   fis8  fis  fis4  fis | % 12
	 fis  dis8  e4  fis4. | % 13
	r4   g8  g  g4  g | % 14
	 fis2  e | % 15
	 d1 
	}
	
}

verse = \lyricmode{
	 \set stanza = #"1. " Ru,   mit   barn   tiu   fü -- le -- schä -- ret   man  
	 -- ge   mo -- ker   fand   wi   daar.         Diss  
	 um   brin -- ger   uss   i   ga -- ve   dröm   öm  
	 ha -- vet.   He -- le   da -- gen   will   wi   wa  
	 -- re   dar   wur   al -- le   mo -- ker   ar.  
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
