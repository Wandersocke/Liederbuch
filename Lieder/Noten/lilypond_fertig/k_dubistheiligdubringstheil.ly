\version "2.18.2"

\include "defines.ly"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

%Hier kommen Einstellungen für das Notensystem hin, als Tempo, Takt usw. Tempo braucht man nur bei der Erstellung von Midi Files
global={
  \key c \major
  \time 4/4
  %\tempo 4=90
}

%Hier kommen Akkorde hin.
chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
   s2 a1:m d:m g c f d:m e e2 e1 a2:m s2 a1:m d:m g c f d:m e e2 e1:7 a2:m
}

%Hier kommen die Noten hin. r ist eine Pause, s eine unsichtbare Pause
mel = \relative c' {
  \global
  \partial 2
  \repeat volta 2 {
  e4^\markup {\upright  "1"} d |
  c4. d8 e c4 f8( |
  f2 ) f4 e |
  d4. e8 f d4 e8( |
  e2 ) e4 d |
  c4. d8 e c4 d8( |
  d2 ) d4 c |
  }
  \alternative {
    {
      b4. b8 b4 b8 e8( |
      \set Timing.measureLength = #(ly:make-moment 2/4)
      e2 ) |
    }
    {
      \set Timing.measureLength = #(ly:make-moment 4/4)
      b4. b8 c b4 a8( |
      \set Timing.measureLength = #(ly:make-moment 2/4)
      a2 ) \bar "||" |
    }
  }
  \break
  
  \repeat volta 2 {
    e'4^\markup {\upright  "2"} e |
    \set Timing.measureLength = #(ly:make-moment 4/4)
    e4. a8( a2 ) |
    r2 a4 a |
    a4. g8( g2 ) |
    r2 g4 g |
    g4. f8( f2 ) |
    r2 f4 f |
  }
  \alternative {
    {
     e4 e e8 e4 gis8( |
     \set Timing.measureLength = #(ly:make-moment 2/4)
     g2 ) |
    }
    {
     \set Timing.measureLength = #(ly:make-moment 4/4)
     e4. e8( e4 ) d |
     e2 \bar "|."
    }
  }
}

%Hier kommt der Text hin, Silben (Noten) per [Leerzeichen]--[Leerzeichen] getrennt.
vers = \lyricmode {
  Du bist hei -- lig, Du bringst Heil,
  bist die Fül -- le, wir ein Teil
  der Ge -- schich -- te, die Du webst,
  Gott, wir dan -- ken Dir, du lebst
  _ _ _ _ _ 
  Du bist hei -- lig, Du bist hei -- lig,
  Du bist hei -- lig, al -- le Welt schau -- e auf Dich.
}

verse = \lyricmode {
  mit -- ten un -- ter uns im Geist,
  der Le -- ben -- dig -- keit ver -- heißt,
  kommst zu uns in Brot und Wein,
  schenkst uns _ _ _ _ _ Dei -- ne Lie -- be ein.
  Hal -- le --lu -- ja, Hal -- le -- lu -- ja,
  Hal -- le -- lu -- ja, Hal -- le -- _ _ _ _ _ lu -- ja für Dich!
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
 \new Staff \with {
	\consists "Volta_engraver" 
        } <<
    \new Voice = "first"
    { \mel }
      \addlyrics {\set fontSize = #+2 \vers}
      \addlyrics {\set fontSize = #+2 \verse}
   >> >>
   \midi{ } 
   \layout {
    \context {
      \Score
      chordRootNamer = #germanChords
      chordNoteNamer = #note-name->german-markup
      \remove "Volta_engraver"
    }
    }
  }
}






