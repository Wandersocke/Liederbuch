\version "2.12.3"

\paper{
  indent=0\mm
  oddFooterMarkup=##f
  oddHeaderMarkup=##f
  bookTitleMarkup = ##f
  scoreTitleMarkup = ##f
}

global={
  \clef treble
  \key g \major 
  \time 4/4 
  \partial 64*32
}

chordNames = \chordmode {
  \override ChordName #'font-series = #'bold
  \global
  \set chordNameLowercaseMinor = ##t
  \germanChords
  % Akkorde folgen hier.
  s2 e1:m e:m d e:m e:m e:m g d e:m d g d e:m e:m g2 d e:m
}

melody = \relative c' {
  \global
	 r4  r8   b | % 2
	 e  e4  e8  e  e4  fis8 | % 3
	 g4.  fis8  e4.  e8 | % 4
	 d4.  d8  d4  e | % 5
	 b2 r4  r8   b | % 6
	 e4  e  e8  e4  fis8 | % 7
	 g4.  fis8  e4.  e8 | % 8
	 b'  b4  b8  b  c4  b8 | % 9
	 a2 r4  r8   a | % 10
	 b  b4  b8  g  g4  g8 | % 11
	 a4  a  fis r8   fis | % 12
	 g4  g  g8(  fis4)  e8 | % 13
	 fis2 r4  r8   b, | % 14
	 e4  e  e8  e4  fis8 | % 15
	 g  g4  fis8  e4.  e8 | % 16
	 b'4.  b8  d,4  fis | % 17
	 e2  
	\bar "|."
	
}

verse = \lyricmode{
	\set stanza = #"1. " Der Win -- ter war mäch -- tig in je -- nem 
	Jahr, das Land war weiß von Schnee. Die Bäu 
	-- me schie -- nen aus Zuc -- ker -- guss, ge -- fro 
	-- ren wo -- hin man nur seh'. Der Fluss war 
	ein glän -- zen -- des Meer aus Eis, das lang 
	-- sam wei -- ter  treibt. Wo wie man sagt, 
	ein ver -- lo -- re -- ner Geist im Win -- ter 
	im -- mer bleibt. 
}

\book {
  \score {
    <<
      \new ChordNames {\set chordChanges = ##t \chordNames}
      \new Staff {\melody}
      \addlyrics {\set fontSize = #+2 \verse}
    >>
    \layout { }
  }
}
