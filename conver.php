<?php

$files = glob(__DIR__ . '/Lieder/z*.tex');

foreach ($files as $file) {

    $path = $file;
    $handle = fopen($path, 'r');

    if (!$handle) {
        throw new \RuntimeException("Can't open file");
    }

    $newFile = [];

    $firstOpenTabbing = true;
    $lastCloseTabbingPosition = null;
    $insideTabbing = false;

    $firstRow = null;
    $secondRow = null;

    $nochords = false;

    $title = "";

    while (($line = fgets($handle)) !== false) {

        if (str_starts_with($line, '%')) {
            $newFile[] = $line;
            continue;
        }

        if (str_contains($line, '{figure}')) {
            continue;
        }

        $line = str_replace(
            ['"a', '"o', '"u', '"s'],
            ['ä', 'ö', 'ü', 'ß'],
            $line
        );

        //var_dump($line);
        if (str_starts_with($line, '\centering \section*')) {
            $title = trim(preg_replace("/.*?\{(.*?)\}.*/", "$1", $line));
            continue;
        }

        if (str_contains($line, '\end{tabbing}')) {
            $line = str_replace('\end{tabbing}', '\endverse', $line);
            $lastCloseTabbingPosition = count($newFile);
            $insideTabbing = false;
            $firstRow = null;
            $secondRow = null;
        }

        if ($insideTabbing) {
            if (empty(trim($line))) {
                continue;
            }

            if ($firstRow === null) {
                if (str_contains($line, '\kill')) {
                    $firstRow = $line;
                    continue;
                }
                $newFile[] = '\chordsoff ' . trim($line) . '\chordson' . PHP_EOL;
                continue;
            }
            if ($secondRow === null && str_contains($line, '\\')) {
                $line = str_replace('\\', '', $line);
                $line = str_replace('bf ', '', $line);

                $secondRow = array_values(array_filter(explode(">", $line)));

                continue;
            }

            if (str_contains($line, '\kill')) {
                var_dump($line);
                throw new \RuntimeException('There are too many \kill at file ' . basename($path));
            }

            $line = "";
            $verseLines = explode('\=', str_replace('\\kill', ' \\\\', $firstRow));

            if (count($verseLines) > count($secondRow) + 1) {
                var_dump($firstRow, $verseLines, $secondRow);
                throw new \RuntimeException('More or less accords than accord positions at file ' . basename($path));
            }

            foreach ($verseLines as $key => $verseLine) {
                if (!isset($secondRow[$key])) {
                    $line .= $verseLine;
                    continue;
                }
                $accord = str_replace('$', '', $secondRow[$key]);
                $line .= $verseLine . '\chord{' . trim($accord) . '}';
            }

            $firstRow = null;
            $secondRow = null;
        }

        if (str_starts_with($line, '\includegraphics')) {
            if ($firstOpenTabbing) {
                $line = '\beginsong{' . $title . '}[]' . PHP_EOL . $line;
                $firstOpenTabbing = false;
            }
        }

        if (str_contains($line, '\begin{tabbing}')) {
            $line = str_replace('\begin{tabbing}', '\beginverse', $line);
            if ($firstOpenTabbing) {
                $line = '\beginsong{' . $title . '}[]' . PHP_EOL . $line;
                $firstOpenTabbing = false;
            }
            $insideTabbing = true;
        }

        $newFile[] = $line;
    }

    if ($lastCloseTabbingPosition !== null) {
        $newFile[$lastCloseTabbingPosition] = $newFile[$lastCloseTabbingPosition] . '\endsong' . PHP_EOL;
    }

    fclose($handle);

    //var_dump(implode('', $newFile));
    file_put_contents($file, implode('', $newFile));
}