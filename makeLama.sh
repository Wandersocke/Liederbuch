#!/bin/bash

rm *.log
rm *.ilg
rm *.ind
rm *.idx
rm *.dat
rm *.aux
rm *.logo
rm ./Lieder/*.aux

java -jar ./Lieder/ChangeAkk.jar
pdflatex lama.tex
java -jar russisch_transliteral.jar
pdflatex lama.tex
makeindex in.idx
makeindex vw.idx
pdflatex lama.tex

rm -R *.log
rm -R *.ilg
rm -R *.ind
rm -R *.idx
rm -R *.dat
rm -R *.aux
rm *.logo
rm ./Lieder/*.aux
